import 'package:gtrack/model/currentVehicle.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  DatabaseHelper.internal();
  static Database _db;
  static const tableName = "vehicles_table";
  static const id = "id";
  static const vehicleName = "vehicleName";
  static const previousTime = "previousTime";
  static const currentTime = "currentTime";
  static const dbName = "vehicles_info.db";
  static const version = 1;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, dbName);
    var db = await openDatabase(path, version: version, onCreate: _onCreate);
    print("Database Path : $db");
    return db;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $tableName ($id INTEGER, $vehicleName TEXT, $previousTime TEXT);");
    print("Table is Created");
  }

// inserting vehicles
  Future<CurrentVehicle> saveVehicle(CurrentVehicle currentVehicle) async {
    var dbClient = await db;
    currentVehicle.id =
        await dbClient.insert(tableName, currentVehicle.toMap());
    return currentVehicle;
  }

  // deletion
  // Future<int> delete(CurrentVehicle currentVehicle) async {
  //   var dbClient = await db;
  //   int result = await dbClient.delete(tableName);
  //   return result;
  // }

  // fetch data
  Future<List<CurrentVehicle>> getData() async {
    var dbClient = await db;
    List<Map> map = await dbClient.rawQuery('SELECT * FROM $tableName');
    print("map : $map");
    List<CurrentVehicle> list = new List();
    if (map.length > 0) {
      for (int i = 0; i < map.length; i++) {
        list.add(CurrentVehicle.fromMap(map[i]));
      }
    }
    return list;
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(tableName, where: '$id = ?', whereArgs: [id]);
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
