import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/actions/companyIssuesAction.dart';
import 'package:gtrack/actions/disposeActions.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/pages/detailIssuesPage.dart';

class IssuesPage extends StatefulWidget {
  @override
  _IssuesPageState createState() => _IssuesPageState();
}

class _IssuesPageState extends State<IssuesPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      onInit: (store) => store.dispatch(IssuesAction().issues(scaffoldKey)),
      onDispose: (store) => store.dispatch(DisposeActions.DisposeIssue),
      converter: (store) => store.state,
      builder: (_, state) {
        return DefaultTabController(
          length: 2,
          child: Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
              elevation: 0,
              title: Text('Issues'),
              backgroundColor: Colors.teal,
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  color: Colors.teal,
                  child: TabBar(
                    indicatorColor: Colors.white,
                    tabs: <Widget>[
                      Tab(
                        text: 'OPEN',
                      ),
                      Tab(text: 'CLOSE'),
                    ],
                  ),
                ),
                Expanded(
                    child: state.issue != null
                        ? TabBarView(
                            children: <Widget>[
                              OpenIssuesPage(
                                  openIssuesList: state.issue.issuesList),
                              ClosedIssuesPage(
                                  closedIssuesList: state.issue.issuesList),
                            ],
                          )
                        : Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          ))
              ],
            ),
          ),
        );
      },
    );
  }
}
