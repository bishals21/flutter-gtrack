import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/utils/textUtils.dart';

class ServicingReportsPage extends StatefulWidget {
  @override
  _ServicingReportsPageState createState() => _ServicingReportsPageState();
}

class _ServicingReportsPageState extends State<ServicingReportsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Container(
          child: state.servicingReportsList == null
              ? Center(
                  child: Container(),
                )
              : state.servicingReportsList.isEmpty
                  ? Center(
                      child: Text('No Report'),
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          color: Colors.grey.withOpacity(0.4),
                          child: Table(
                            columnWidths: {0: FixedColumnWidth(130.0)},
                            children: [
                              TableRow(children: [
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8.0),
                                    child: Text(
                                      'TARGET NAME',
                                      textAlign: TextAlign.left,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'DATE',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'TYPE',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'DISTANCE',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'REMARKS',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                              ])
                            ],
                          ),
                        ),

                        // Container(
                        //   padding: EdgeInsets.symmetric(horizontal: 8),
                        //   child: Table(
                        //       columnWidths: {0: FixedColumnWidth(150.0)},
                        //       children: distanceFuelReportList.map((item) {
                        //         DistanceFuelReport distanceFuelReport = item;

                        //         return TableRow(children: [
                        //           TableCell(
                        //             child: Padding(
                        //               padding: const EdgeInsets.symmetric(
                        //                 vertical: 8.0,
                        //               ),
                        //               child: Text(
                        //                 distanceFuelReport.targetName,
                        //                 style: TextUtils.titleStyle,
                        //               ),
                        //             ),
                        //           ),
                        //           TableCell(
                        //             child: Padding(
                        //               padding: const EdgeInsets.symmetric(vertical: 8.0),
                        //               child: Text(
                        //                 'distanceFuelReport',
                        //                 textAlign: TextAlign.center,
                        //                 style: TextUtils.titleStyle,
                        //               ),
                        //             ),
                        //           ),
                        //           TableCell(
                        //             child: Padding(
                        //               padding: const EdgeInsets.symmetric(vertical: 8.0),
                        //               child: Text(
                        //                 'distanceFuelReport',
                        //                 textAlign: TextAlign.center,
                        //                 style: TextUtils.titleStyle,
                        //               ),
                        //             ),
                        //           ),
                        //           TableCell(
                        //             child: Padding(
                        //               padding: const EdgeInsets.symmetric(vertical: 8.0),
                        //               child: Text(
                        //                 'distanceFuelReport',
                        //                 textAlign: TextAlign.center,
                        //                 style: TextUtils.titleStyle,
                        //               ),
                        //             ),
                        //           ),
                        //           TableCell(
                        //             child: Padding(
                        //               padding: const EdgeInsets.symmetric(vertical: 8.0),
                        //               child: Text(
                        //                 '',
                        //                 textAlign: TextAlign.center,
                        //                 style: TextUtils.titleStyle,
                        //               ),
                        //             ),
                        //           ),
                        //         ]);
                        //       }).toList()),
                        // ),
                      ],
                    ),
        );
      },
    );
  }
}
