import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:gtrack/actions/companyAction.dart';
import 'package:gtrack/actions/disposeActions.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/custom_package/linearPercentageIndicator.dart';
import 'package:gtrack/model/vehicle.dart';

class WatchRoutesPage extends StatefulWidget {
  final DateTime fromDate;
  final DateTime toDate;
  final String deviceId;
  WatchRoutesPage({this.fromDate, this.toDate, this.deviceId});
  @override
  _WatchRoutesPageState createState() => _WatchRoutesPageState();
}

class _WatchRoutesPageState extends State<WatchRoutesPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  GoogleMapController mapController;
  CameraPosition cameraPosition = CameraPosition(
    target: LatLng(27.7172, 85.3240),
    zoom: 12.0,
  );
  Set<Polyline> polyline = {};
  Set<Marker> marker = {};
  List<LatLng> routeHistoryList = new List();
  int currentIndex = 0;
  bool isStart = false;
  //

  int interval = 1;

  Timer timer;
  List<VehiclePosition> vehiclePositionList;

  // int markerIndex;
  int start;
  double routePercentage = 0.0;

  _calculateRoutePercentage() {
    if (routePercentage == 100.0) {
      routePercentage = 0.0;
    } else {
      routePercentage =
          (currentIndex.toDouble() / routeHistoryList.length.toDouble());
    }
    setState(() {});
  }

  // adding here

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
   _getRoutes();
  }

  _getRoutes() {
    StoreProvider.of<AppState>(context).state.routeHistoryList.forEach((item) {
      routeHistoryList.add(
        LatLng(
          double.parse(item.latitude),
          double.parse(item.longitude),
        ),
      );
    });
    start = routeHistoryList.length;
    setState(() {});
    _placePolyline();

    print("After : ${routeHistoryList.length}");
  }

  _placePolyline() {
    polyline.add(
      Polyline(
        polylineId: PolylineId("1"),
        color: Colors.red,
        jointType: JointType.mitered,
        width: 2,
        points: routeHistoryList,
      ),
    );
    _placeMarker();
  }

  _placeMarker() {
    marker.clear();
    marker.add(
      Marker(
        markerId: MarkerId("1"),
        position: LatLng(routeHistoryList[currentIndex].latitude,
            routeHistoryList[currentIndex].longitude),
      ),
    );
    cameraPosition = CameraPosition(
      target: LatLng(routeHistoryList[currentIndex].latitude,
          routeHistoryList[currentIndex].longitude),
      zoom: 25,
    );
    mapController.animateCamera(
      CameraUpdate.newLatLngZoom(
          LatLng(routeHistoryList[currentIndex].latitude,
              routeHistoryList[currentIndex].longitude),
          18),
    );
  }

  _startTimer() {
    var oneSec = Duration(seconds: interval);
    timer = Timer.periodic(
        oneSec,
        (Timer timer) => setState(() {
              print("marker index : $currentIndex");
              if (start < 1) {
                timer.cancel();
                isStart = false;
                start = routeHistoryList.length;
                currentIndex = 0;
                routePercentage = 0.0;
                _placeMarker();
              } else {
                start = start - 1;
                print("Time : $start");
                currentIndex++;
                _placeMarker();
                isStart = true;
                _calculateRoutePercentage();
                // marker.clear();
              }
            }));
  }

  _pauseTimer() {
    timer.cancel();
    isStart = false;
    setState(() {});
    print("Current Index : $currentIndex");
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if (timer != null) {
      timer.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      onInit: (store) => store.dispatch(VehicleRoutesAction().historyRoutes(
          scaffoldKey, widget.deviceId, widget.fromDate, widget.toDate)),
      onDispose: (store) =>
          store.dispatch(DisposeActions.DisposeRouteHistoryList),
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.teal,
            title: Text('Watch Routes'),
          ),
          body: Center(
            child: state.routeHistoryList != null
                ? state.routeHistoryList.isNotEmpty
                    ? SizedBox(
                        width: double.infinity,
                        child: Stack(
                          children: <Widget>[
                            GoogleMap(
                              onMapCreated: _onMapCreated,
                              initialCameraPosition: cameraPosition,
                              rotateGesturesEnabled: true,
                              scrollGesturesEnabled: true,
                              tiltGesturesEnabled: true,
                              zoomGesturesEnabled: true,
                              polylines: polyline,
                              markers: marker,
                            ),
                            Align(
                                alignment: Alignment.bottomCenter,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: 70,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8),
                                    width: double.infinity,
                                    color: Colors.grey.withOpacity(0.4),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        LinearPercentIndicator(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              35,
                                          animation: true,
                                          lineHeight: 10.0,
                                          animationDuration: 2000,
                                          percent: routePercentage,
                                          animateFromLastPercent: true,
                                          center: Text(
                                            "${(routePercentage * 100.0).toStringAsFixed(2)}",
                                          ),
                                          linearStrokeCap:
                                              LinearStrokeCap.roundAll,
                                          progressColor: Colors.white,
                                        ),
                                        Center(
                                          child: IconButton(
                                            onPressed: () {
                                              isStart
                                                  ? _pauseTimer()
                                                  : _startTimer();
                                            },
                                            icon: Icon(
                                              isStart
                                                  ? Icons.pause
                                                  : Icons.play_arrow,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      )
                    : Text('No Route Found')
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(),
                  ),
          ),
        );
      },
    );
  }
}
