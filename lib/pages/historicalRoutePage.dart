 import 'package:flutter/material.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/department.dart';
import 'package:gtrack/pages/watchRoutes.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class HistoricalRoutePage extends StatefulWidget {
  final List<Department> departmentList;
  HistoricalRoutePage({this.departmentList});
  @override
  _HistoricalRoutePageState createState() => _HistoricalRoutePageState();
}

class _HistoricalRoutePageState extends State<HistoricalRoutePage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  DateTime fromDate;
  DateTime toDate;
  List<DropdownMenuItem<DepartmentVehicles>> departmentVehiclesItems;
  DepartmentVehicles departmentVehicles;

  void changeVehicles(DepartmentVehicles d) {
    setState(() {
      departmentVehicles = d;
      // deviceId = departmentVehicles.deviceId.toString();
    });
  }

  List<DropdownMenuItem<DepartmentVehicles>> getDepartmentVehicles() {
    List<DropdownMenuItem<DepartmentVehicles>> items = new List();
    final vehiclesList = widget.departmentList;

    vehiclesList.forEach((item) {
      if (item.departmentVehiclesList.isNotEmpty) {
        item.departmentVehiclesList.forEach((department) {
          items.add(DropdownMenuItem(
            value: department,
            child: Text(department.deviceName),
          ));
        });
      }
    });

    return items;
  }

  @override
  void initState() {
    departmentVehiclesItems = getDepartmentVehicles();
    departmentVehicles = departmentVehiclesItems[0].value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.teal,
            title: Text('Hisotrical Routes'),
          ),
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                DateTimePickerFormField(
                  inputType: InputType.both,
                  format: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
                  editable: true,
                  decoration: InputDecoration(
                      labelText: 'From', hasFloatingPlaceholder: true),
                  firstDate: DateTime(1990),
                  onChanged: (dt) => setState(() => fromDate = dt),
                ),
                DateTimePickerFormField(
                  inputType: InputType.both,
                  format: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
                  editable: true,
                  decoration: InputDecoration(
                      labelText: 'To', hasFloatingPlaceholder: true),
                  firstDate: DateTime(1990),
                  onChanged: (dt) => setState(() => toDate = dt),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      "Vehicle",
                      style: TextStyle(fontSize: 16),
                    ),
                    DropdownButton(
                      value: departmentVehicles,
                      items: departmentVehiclesItems,
                      onChanged: changeVehicles,
                    ),
                  ],
                ),
                MaterialButton(
                  onPressed: () {
                    if (fromDate == null || toDate == null) {
                      DialoguesUtils.showSnackBar(
                          scaffoldKey, 'Enter both dates');
                    } else {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => WatchRoutesPage(
                                fromDate: fromDate,
                                toDate: toDate,
                                deviceId: departmentVehicles.id.toString(),
                              ),
                        ),
                      );
                    }
                  },
                  color: Colors.teal,
                  child: Center(
                    child:
                        Text('Search', style: TextStyle(color: Colors.white)),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
