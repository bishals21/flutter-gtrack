import 'package:flutter/material.dart';
import 'package:gtrack/actions/loginAction.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
 
import 'package:flutter_redux/flutter_redux.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  String username = '';
  String password = '';

  void postLogin() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      DialoguesUtils.showProgressDialogue(context, 'Logging');
      StoreProvider.of<AppState>(context)
          .dispatch(LoginAction().userLogin(scaffoldKey, username, password));
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(16),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  height: 60,
                  width: MediaQuery.of(context).size.width - 50,
                  child:
                      Center(child: Image.asset('assets/trackometer_logo.png')),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Text(
                  'Login to your account',
                  style: TextStyle(fontSize: 16),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Text(
                  'Enter your credentials',
                  style: TextStyle(fontSize: 14),
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                ),
                Form(
                  key: formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        autofocus: false,
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) return 'Enter Username';
                        },
                        onSaved: (value) {
                          username = value;
                        },
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(horizontal: 4),
                            labelText: 'Username',
                            hintText: 'Enter username',
                            suffixIcon: Icon(Icons.person)),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                      ),
                      TextFormField(
                        autofocus: false,
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value.isEmpty) return 'Enter Password';
                        },
                        onSaved: (value) {
                          password = value;
                        },
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(horizontal: 4),
                            labelText: 'Password',
                            hintText: 'Enter Password',
                            suffixIcon: Icon(Icons.lock)),
                      ),
                      Padding(
                        padding: EdgeInsets.all(16),
                      ),
                      GestureDetector(
                        onTap: postLogin,
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(12),
                            color: Colors.teal,
                            child: Center(
                              child: Text('LOGIN',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  )),
                            )),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
