import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/overallReports.dart';
import 'package:gtrack/utils/textUtils.dart';

class OverallReportsPage extends StatefulWidget {
  @override
  _OverallReportsPageState createState() => _OverallReportsPageState();
}

class _OverallReportsPageState extends State<OverallReportsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Container(
          child: state.overallReportsList == null
              ? Center(child: Container())
              : state.overallReportsList.isEmpty
                  ? Center(
                      child: Text('No Data '),
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          color: Colors.grey.withOpacity(0.4),
                          child: Table(
                            columnWidths: {0: FixedColumnWidth(150.0)},
                            children: [
                              TableRow(children: [
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8.0),
                                    child: Text(
                                      'TARGET NAME',
                                      textAlign: TextAlign.left,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'DISTANCE (km)',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'AVE SPEED',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'MAX SPEED',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'FUEL CONSM',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                              ])
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          child: Table(
                              columnWidths: {0: FixedColumnWidth(150.0)},
                              children: state.overallReportsList.map((item) {
                                OverallReports overallReports = item;

                                return TableRow(children: [
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 8.0,
                                      ),
                                      child: Text(
                                        overallReports.name,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        overallReports.totalDistance.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        overallReports.speedSum.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        overallReports.speedMax.toString(),
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        '',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                ]);
                              }).toList()),
                        ),
                      ],
                    ),
        );
      },
    );
  }
}
