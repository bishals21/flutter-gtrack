import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/actions/alertsAction.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/eventAlerts.dart';
import 'package:gtrack/model/maintenanceAlerts.dart';

class EventAlertsPage extends StatefulWidget {
  @override
  _EventAlertsPageState createState() => _EventAlertsPageState();
}

class _EventAlertsPageState extends State<EventAlertsPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      // onInit: (store) =>
      //     store.dispatch(EventAlertsAction().eventAlerts(scaffoldKey)),
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
            key: scaffoldKey,
            body: state.eventAlerts != null
                ? state.eventAlerts.eventAlertsList.isNotEmpty
                    ? ListView.builder(
                        itemCount: state.eventAlerts.eventAlertsList.length,
                        itemBuilder: (_, index) {
                          if (index + 1 ==
                              state.eventAlerts.eventAlertsList.length) {
                            if (state.eventAlerts.nextPage == 0 ||
                                state.eventAlerts.nextPage == null) {
                              //  print('No more data');

                            } else {
                              StoreProvider.of<AppState>(context).dispatch(
                                  LoadMoreAction().loadMoreEvents(
                                      scaffoldKey,
                                      StoreProvider.of<AppState>(context)
                                          .state
                                          .eventAlerts
                                          .nextPage
                                          .toString()));
                            }
                          }
                          Alert alert =
                              state.eventAlerts.eventAlertsList[index];

                          return ListTile(
                            // leading: Text(alert.vehicle),
                            title: Text(
                              alert.vehicle,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            subtitle: Text(alert.type),
                            trailing: Text(alert.time),
                          );
                        },
                      )
                    : Center(
                        child:
                            Text('No Alerts', style: TextStyle(fontSize: 16)),
                      )
                : Center(
                    child: CircularProgressIndicator(),
                  ));
      },
    );
  }
}

class MaintenanceAlertsPage extends StatefulWidget {
  @override
  _MaintenanceAlertsPageState createState() => _MaintenanceAlertsPageState();
}

class _MaintenanceAlertsPageState extends State<MaintenanceAlertsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      // onInit: (store) => store
      //     .dispatch(MaintenanceAlertsAction().maintenanceAlerts(scaffoldKey)),
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
            body: state.maintenanceAlertsList == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : state.maintenanceAlertsList.isEmpty
                    ? Center(
                        child:
                            Text('No Alerts', style: TextStyle(fontSize: 16)),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: state.maintenanceAlertsList.map((item) {
                          MaintenanceAlerts maintenanceAlerts = item;
                          return Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: ListTile(
                              title: Text(
                                maintenanceAlerts.name,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(maintenanceAlerts.message,
                                  style: TextStyle(
                                    fontSize: 14,
                                  )),
                            ),
                          );
                        }).toList()));
      },
    );
  }
}
