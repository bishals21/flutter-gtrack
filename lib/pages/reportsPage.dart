import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/actions/disposeActions.dart';
import 'package:gtrack/actions/reportsAction.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/department.dart';
import 'package:gtrack/pages/distanceFuelReportsPage.dart';
import 'package:gtrack/pages/overallReportsPage.dart';
import 'package:gtrack/pages/overspeedReportsPage.dart';
import 'package:gtrack/pages/servicingReportsPage.dart';
import 'package:gtrack/pages/tripReportsPage.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:intl/intl.dart';

class ReportsPage extends StatefulWidget {
  final List<Department> departmentList;
  ReportsPage({this.departmentList});
  @override
  _ReportsPageState createState() => _ReportsPageState();
}

class _ReportsPageState extends State<ReportsPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final titleStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 12);
  List<DropdownMenuItem<String>> dropDownMenuItems;
  List<DropdownMenuItem<DepartmentVehicles>> departmentVehiclesItems;
  DepartmentVehicles departmentVehicles;
  String currentTitle;
  DateTime fromDate;
  DateTime toDate;
  var fuelConsumed = '';
  String deviceId = '';
  Widget childWidget = Container();

  List<String> titleList = [
    'Overall',
    'Distance/Fuel',
    'Trip',
    'Overspeed',
    'Servicing Log'
  ];

  @override
  void initState() {
    dropDownMenuItems = getDropDownMenuItems();
    currentTitle = dropDownMenuItems[0].value;

    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String title in titleList) {
      items.add(new DropdownMenuItem(value: title, child: new Text(title)));
    }
    return items;
  }

  void changedDropDownItem(String selectedTitle) {
    setState(() {
      currentTitle = selectedTitle;
      if (currentTitle == "Overall" || currentTitle == "Distance/Fuel") {
        deviceId = '';
      } else {
        departmentVehiclesItems = getDepartmentVehicles();
        departmentVehicles = departmentVehiclesItems[0].value;
        deviceId = departmentVehicles.deviceId.toString();
      }
    });
  }

  List<DropdownMenuItem<DepartmentVehicles>> getDepartmentVehicles() {
    List<DropdownMenuItem<DepartmentVehicles>> items = new List();
    final vehiclesList = widget.departmentList;

    vehiclesList.forEach((item) {
      if (item.departmentVehiclesList.isNotEmpty) {
        item.departmentVehiclesList.forEach((department) {
          items.add(DropdownMenuItem(
            value: department,
            child: Text(department.deviceName),
          ));
        });
      }
    });

    return items;
  }

  void changeVehicles(DepartmentVehicles d) {
    setState(() {
      departmentVehicles = d;
      deviceId = departmentVehicles.deviceId.toString();
    });
  }

  dispatchActions() {
    fromDate = fromDate != null
        ? fromDate
        : DateTime.now().subtract(Duration(days: 1));
    toDate = toDate != null ? toDate : DateTime.now();
    if (currentTitle == 'Overall') {
      StoreProvider.of<AppState>(context).dispatch(
          OverallReportsAction().overallReports(scaffoldKey, fromDate, toDate));
    } else if (currentTitle == 'Distance/Fuel') {
      StoreProvider.of<AppState>(context).dispatch(DistanceFuelReportAction()
          .distanceFuelReport(scaffoldKey, fromDate, toDate));
    } else if (currentTitle == 'Trip') {
      StoreProvider.of<AppState>(context).dispatch(TripReportActions()
          .tripReports(scaffoldKey, deviceId, fromDate, toDate));
    } else if (currentTitle == 'Overspeed') {
      StoreProvider.of<AppState>(context).dispatch(OverSpeedReportsAction()
          .overSpeedReports(scaffoldKey, deviceId, fromDate, toDate));
    } else if (currentTitle == 'Servicing Log') {
      StoreProvider.of<AppState>(context).dispatch(ServicingReportsAction()
          .servicingReports(scaffoldKey, deviceId, fromDate, toDate));
    }
  }

  getChildWidget() {
    if (currentTitle == 'Overall') {
      childWidget = OverallReportsPage();
    } else if (currentTitle == 'Distance/Fuel') {
      childWidget = DistanceFuelReportsPage();
    } else if (currentTitle == 'Trip') {
      childWidget = TripReportsPage();
    } else if (currentTitle == 'Overspeed') {
      childWidget = OverspeedReportsPage();
    } else if (currentTitle == 'Servicing Log') {
      childWidget = ServicingReportsPage();
    }

    return childWidget;
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>( 
      onDispose: (store) {
        store.dispatch(DisposeActions.DisposeOverallReportsList);
        store.dispatch(DisposeActions.DisposeDistanceFuelReportsList);
        store.dispatch(DisposeActions.DisposeTripReportsList);
        store.dispatch(DisposeActions.DisposeOverSpeedReportsList);
        store.dispatch(DisposeActions.DisposeMaintenanceAlertsList);
      },
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            title: Text('Reports'),
            backgroundColor: Colors.teal,
          ),
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            "Report Title",
                            style: TextStyle(fontSize: 16),
                          ),
                          DropdownButton(
                            value: currentTitle,
                            items: dropDownMenuItems,
                            onChanged: changedDropDownItem,
                          ),
                        ],
                      ),
                      (currentTitle == 'Overall' ||
                              currentTitle == 'Distance/Fuel')
                          ? Container()
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  "Vehicle",
                                  style: TextStyle(fontSize: 16),
                                ),
                                DropdownButton(
                                  value: departmentVehicles,
                                  items: departmentVehiclesItems,
                                  onChanged: changeVehicles,
                                ),
                              ],
                            ),
                    ],
                  ),
                  DateTimePickerFormField(
                    inputType: InputType.both,
                    format: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
                    editable: true,
                    initialValue: DateTime.now().subtract(Duration(days: 1)),
                    decoration: InputDecoration(
                        labelText: 'From', hasFloatingPlaceholder: true),
                    firstDate: DateTime(1990),
                    onChanged: (dt) => setState(() => fromDate = dt),
                  ),
                  DateTimePickerFormField(
                    inputType: InputType.both,
                    format: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
                    editable: true,
                    initialValue: DateTime.now(),
                    decoration: InputDecoration(
                        labelText: 'To', hasFloatingPlaceholder: true),
                    firstDate: DateTime(1990),
                    onChanged: (dt) => setState(() => toDate = dt),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: () {
                        getChildWidget();
                        dispatchActions();
                        DialoguesUtils.showSnackBar(
                            scaffoldKey, 'Please Wait...');
                      },
                      child: Chip(
                        label: Text(
                          'Filter',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        shape: BeveledRectangleBorder(),
                        backgroundColor: Colors.teal,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                  ),
                  childWidget,
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
