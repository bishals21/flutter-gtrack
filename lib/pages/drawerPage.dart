import 'package:flutter/material.dart';
import 'package:gtrack/actions/loginAction.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/pages/alertsPage.dart';
import 'package:gtrack/pages/historicalRoutePage.dart';
import 'package:gtrack/pages/issuesPage.dart';
import 'package:gtrack/pages/loginPage.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/pages/reportsPage.dart';

class UserInfoPage extends StatefulWidget {
  @override
  _UserInfoPageState createState() => _UserInfoPageState();
}

class _UserInfoPageState extends State<UserInfoPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final textStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          body: Container(
            child: Stack(
              children: <Widget>[
                Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.45,
                    color: Colors.teal,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.network(
                          state.user.image,
                          height: 70,
                          width: 150,
                        ),
                        Padding(
                          padding: EdgeInsets.all(8),
                        ),
                        Text(
                          state.user.name,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Align(
                  alignment: Alignment(0.0, 0.2),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    margin: EdgeInsets.only(left: 16, right: 16),
                    height: 240,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 5,
                            color: Colors.black45,
                          )
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.person,
                              size: 24,
                            ),
                            Padding(
                              padding: EdgeInsets.all(4),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Username',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(4),
                                ),
                                Text(
                                  state.user.username,
                                  style: textStyle,
                                )
                              ],
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Divider(
                            color: Colors.black45,
                            height: 1,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.phone,
                              size: 24,
                            ),
                            Padding(
                              padding: EdgeInsets.all(4),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Contact Number',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(4),
                                ),
                                Text(
                                  state.user.contact,
                                  style: textStyle,
                                )
                              ],
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Divider(
                            color: Colors.black45,
                            height: 1,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              size: 24,
                            ),
                            Padding(
                              padding: EdgeInsets.all(4),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Address',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(4),
                                ),
                                Text(
                                  state.user.address,
                                  style: textStyle,
                                )
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class DrawerMenuPage extends StatefulWidget {
  @override
  _DrawerMenuPageState createState() => _DrawerMenuPageState();
}

class _DrawerMenuPageState extends State<DrawerMenuPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  List<Options> optionsList = [
    Options('assets/geofence_icon.png', 'Issues'),
    Options('assets/geofence_icon.png', 'Historical Route'),
    Options('assets/geofence_icon.png', 'Reports'),
    Options('assets/geofence_icon.png', 'Alerts'),
    Options('assets/geofence_icon.png', 'Logout'),
  ];

  showLogoutDialogue() {
    showDialog(
        context: context,
        builder: (context) {
          return new AlertDialog(
            title: new Text("Logout"),
            content: new Text("Do you want to logout ?"),
            actions: <Widget>[
              new FlatButton(
                child: new Text(
                  "Cancel",
                  style: new TextStyle(
                    color: Colors.red,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              new FlatButton(
                child: new Text("OK"),
                onPressed: () {
                  StoreProvider.of<AppState>(context)
                      .dispatch(LogoutAction().logoutUser());
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => LoginPage()),
                      (Route<dynamic> route) => false);
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 200,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/header_bg.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(16),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  child: GridView.count(
                      childAspectRatio: 1.5,
                      crossAxisCount: 2,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 8.0,
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                      children: List.generate(optionsList.length, (index) {
                        Options options = optionsList[index];
                        return InkWell(
                          onTap: () {
                            if (index == 0) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => IssuesPage()));
                            } else if (index == 1) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HistoricalRoutePage(
                                            departmentList:
                                                state.departmentList,
                                          )));
                            } else if (index == 2) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ReportsPage(
                                          departmentList:
                                              state.departmentList)));
                            } else if (index == 3) {
                              Navigator.of(context).pop();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => AlertsPage(),
                                ),
                              );
                            } else if (index == 4) {
                              showLogoutDialogue();
                            }
                          },
                          child: Column(
                            children: <Widget>[
                              ClipOval(
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(options.image))),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(4),
                              ),
                              Text(options.title)
                            ],
                          ),
                        );
                      })),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class Options {
  String image;
  String title;
  Options(this.image, this.title);
}
