import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:gtrack/actions/companyAction.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/db/databaseHelper.dart';
import 'package:gtrack/model/currentPosition.dart';
import 'package:gtrack/model/currentVehicle.dart';
import 'package:gtrack/model/department.dart';
import 'package:gtrack/model/startValue.dart';
import 'package:gtrack/pages/drawerPage.dart';
import 'package:web_socket_channel/io.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  var db;
  List<StartValue> startValuesList = new List();
  int interval = 1;
  int start = 600;
  String vehicleStatus = "";

  final lat = 27.700769;
  final lng = 85.300140;
  double zoomValue = 14.0;
  String _selectedView = 'Normal';
  MapType mapType = MapType.normal;
  IOWebSocketChannel channel;
  GoogleMapController controller;
  CameraPosition cameraPosition = CameraPosition(
    target: LatLng(27.7172, 85.3240),
    zoom: 10,
  );

  String deviceName = '';
  VehicleCurrentPosition currentVehicle;

// adding here
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  VehicleCurrentPosition vehicleCurrentPosition;

//

  mapViewType() {
    switch (_selectedView) {
      case 'None':
        mapType = MapType.none;
        break;
      case 'Normal':
        mapType = MapType.normal;
        break;
      case 'Hybrid':
        mapType = MapType.hybrid;
        break;
      case 'Satellite':
        mapType = MapType.satellite;
        break;
      case 'Terrain':
        mapType = MapType.terrain;
        break;
      default:
        mapType = MapType.normal;
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  static const kDuration = const Duration(milliseconds: 300);

  static const kCurve = Curves.ease;
  final pageController = new PageController();
  final List<Widget> _pages = <Widget>[
    UserInfoPage(),
    DrawerMenuPage(),
  ];

  @override
  void dispose() {
    if (channel != null) {
      channel.sink.close();
    }

    super.dispose();
  }

  updateCheckValues(bool value, DepartmentVehicles departmentVehicles,
      int departmentIndex, int vehicleIndex) {
    // var vehicle = departmentVehicles;

    departmentVehicles.isChecked = value;
    var departmentList =
        StoreProvider.of<AppState>(context).state.departmentList;
    departmentList[departmentIndex]
        .departmentVehiclesList
        .removeAt(vehicleIndex);
    departmentList[departmentIndex]
        .departmentVehiclesList
        .insert(vehicleIndex, departmentVehicles);

    setState(() {});
    //_updateStartValue(departmentVehicles, 600);
    _updateMarker(value, departmentVehicles, departmentIndex);
  }

  // adding here
  // _updateStartValue(DepartmentVehicles departmentVehicles, int value) {
  //   if (startValuesList.isNotEmpty) {
  //     startValuesList.forEach((item) {
  //       if (item.id == departmentVehicles.deviceId.toString()) {
  //         int index = startValuesList.indexOf(item);
  //         StartValue startValue = item;
  //         startValue.start = value;
  //         startValuesList.removeAt(index);
  //         startValuesList.insert(index, startValue);
  //       }
  //     });
  //   } else {
  //     startValuesList
  //         .add(StartValue(departmentVehicles.deviceId.toString(), value));
  //   }
  //   setState(() {});
  // }

  _updateMarker(
      bool value, DepartmentVehicles departmentVehicles, int departmentIndex) {
    if (value) {
      if (int.parse(departmentVehicles.speed) > 0) {
        vehicleStatus = "(Moving)";
      } else {
        vehicleStatus = "(Stopped)";
      }
      setState(() {});
      final markerId = MarkerId(departmentVehicles.deviceId.toString());

      final Marker marker = Marker(
        icon: BitmapDescriptor.fromAsset(
            departmentVehicles.event == "deviceOnline"
                ? Platform.isIOS
                    ? "assets/car/green_car_ios.png"
                    : "assets/car/green_car.png"
                : Platform.isIOS
                    ? "assets/car/red_car_ios.png"
                    : "assets/car/red_car.png"),
        markerId: markerId,
        position: LatLng(
          double.parse(departmentVehicles.latitude),
          double.parse(departmentVehicles.longitude),
        ),
        anchor: Offset(0.5, 0.5),
        infoWindow: InfoWindow(
          title: departmentVehicles.deviceName + " " + vehicleStatus,
          snippet: departmentVehicles.address,
        ),
      );

      markers[markerId] = marker;
      cameraPosition = CameraPosition(
        target: LatLng(double.parse(departmentVehicles.latitude),
            double.parse(departmentVehicles.longitude)),
        zoom: 16.0,
      );
      mapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      _requestSocketConnection(departmentVehicles, departmentIndex);
    } else {
      if (markers
          .containsKey(MarkerId(departmentVehicles.deviceId.toString()))) {
        markers.remove(MarkerId(departmentVehicles.deviceId.toString()));
      }
    }
  }

  _requestSocketConnection(
      DepartmentVehicles departmentVehicles, int departmentIndex) {
    channel = IOWebSocketChannel.connect(
        StoreProvider.of<AppState>(context).state.user.webSocketUrl,
        pingInterval: Duration(seconds: 3));

    if (!departmentVehicles.isChecked) {
      return;
    }

    channel.sink.add(
      departmentVehicles.deviceUniqueId,
    );

    channel.stream.listen((data) {
      setState(() {
        final jsonResponse = json.decode(data);
        print("Response : $jsonResponse");

        final positions = jsonResponse['positions'];

        int id = positions['id'];

        DepartmentVehicles vehicles;
        StoreProvider.of<AppState>(context)
            .state
            .departmentList[departmentIndex]
            .departmentVehiclesList
            .forEach((item) {
          if (item.deviceId == id) {
            vehicles = item;
          }
        });
        setState(() {});
        if (vehicles == null) {
          return;
        }
        if (!vehicles.isChecked) {
          return;
        }

        print("Speed is : ${positions['speed']}");
        // saveInDb(positions);

        vehicleCurrentPosition = VehicleCurrentPosition(
          positions['deviceTime'].toString(),
          positions['fixTime'].toString(),
          positions['altitude'],
          positions['speed'],
          positions['course'],
          positions['latitude'],
          positions['longitude'],
          positions['address'].toString(),
          positions['protocol'].toString(),
          positions['id'],
          positions['deviceUniqueId'].toString(),
          positions['valid'],
          positions['event'].toString(),
        );

        setState(() {});

        if (positions['speed'] > 0) {
          //  vehicleStatus = "(Moving)";
          removeData(positions);
        } else {
          // vehicleStatus = "(Stopped)";

          saveInDb(positions);
        }

        // _showCurrentVehiclePosition(vehicleCurrentPosition);
      });
    });
  }

  removeData(dynamic positions) async {
    var data = await db.getData();
    if (data.isNotEmpty) {
      await db.delete(positions['id']);

      await db.getData();
    }
    vehicleStatus = "(Moving)";
    setState(() {});
    _showCurrentVehiclePosition(vehicleCurrentPosition);
  }

  // save in database
  saveInDb(dynamic positions) async {
    var data = await db.getData();
    if (data.isEmpty) {
      var saveResult = db.saveVehicle(CurrentVehicle(
          positions['id'], deviceName, DateTime.now().toString()));
      print("Save Result : $saveResult");
    } else {
      int timeDifference;
      data.forEach((item) {
        if (item.id == positions['id']) {
          timeDifference = DateTime.now()
              .difference(DateTime.parse(item.previousTime))
              .inMinutes;
        } else {
          var saveResult = db.saveVehicle(CurrentVehicle(
              positions['id'], deviceName, DateTime.now().toString()));
          print("New Result : $saveResult");
        }
      });
      if (timeDifference != null) {
        if (timeDifference <= 10) {
          vehicleStatus = "(Stopped)";
        } else if (timeDifference > 10) {
          vehicleStatus = "(Parked)";
          removeData(positions);
        }
        print("Time difference is : $timeDifference");
      } else {
        print("Device not found");
      }
    }
    setState(() {});
    _showCurrentVehiclePosition(vehicleCurrentPosition);
  }

  _showCurrentVehiclePosition(VehicleCurrentPosition vehicleCurrentPosition) {
    if (markers.containsKey(MarkerId(vehicleCurrentPosition.id.toString()))) {
      markers.remove(MarkerId(vehicleCurrentPosition.id.toString()));
    }
    final markerId = MarkerId(vehicleCurrentPosition.id.toString());

    final Marker marker = Marker(
      markerId: markerId,
      icon: BitmapDescriptor.fromAsset(
        vehicleCurrentPosition.event == "Online"
            ? Platform.isIOS
                ? "assets/car/green_car_ios.png"
                : "assets/car/green_car.png"
            : Platform.isIOS
                ? "assets/car/red_car_ios.png"
                : "assets/car/red_car.png",
      ),
      anchor: Offset(0.5, 0.5),
      position: LatLng(
          vehicleCurrentPosition.latitude, vehicleCurrentPosition.longitude),
      infoWindow: InfoWindow(
        title: deviceName + " " + vehicleStatus,
        snippet: vehicleCurrentPosition.address,
      ),
    );
    markers[markerId] = marker;
    cameraPosition = CameraPosition(
      target: LatLng(
          vehicleCurrentPosition.latitude, vehicleCurrentPosition.longitude),
      zoom: 16,
    );
    mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  // _startTimer() {
  //   print("Starting timer : $start");
  //   var oneSec = Duration(seconds: interval);
  //   timer = Timer.periodic(
  //       oneSec,
  //       (Timer timer) => setState(() {
  //             if (start == 1) {
  //               vehicleStatus = "(Stoppped)";
  //             } else if (start < 1) {
  //               vehicleStatus = "(Parked)";
  //               //  _pauseTimer();
  //             } else {
  //               start = start - 1;
  //               vehicleStatus = "(Moving)";
  //               print("Time : $start");
  //             }
  //           }));
  // }

  @override
  void initState() {
    super.initState();
    db = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      onInit: (store) =>
          store.dispatch(DepartmentAction().companyDepartments(scaffoldKey)),
      // onDispose: (store)=>store.dispatch(DisposeActions.DisposeDepartmentList),
      converter: (store) => store.state,

      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          drawer: Drawer(
            elevation: 1.0,
            child: Stack(
              children: <Widget>[
                PageView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  controller: pageController,
                  itemCount: _pages.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _pages[index];
                  },
                ),
                new Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  right: 0.0,
                  child: new Container(
                    color: Colors.grey[800].withOpacity(0.0),
                    padding: const EdgeInsets.all(20.0),
                    child: new Center(
                      child: new DotsIndicator(
                        color: Colors.grey,
                        controller: pageController,
                        itemCount: _pages.length,
                        onPageSelected: (int page) {
                          pageController.animateToPage(
                            page,
                            duration: kDuration,
                            curve: kCurve,
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          appBar: AppBar(
            title: Text('Real Time Track'),
            actions: <Widget>[
              new PopupMenuButton(
                onSelected: (value) => setState(() {
                  _selectedView = value;
                  mapViewType();
                }),
                child: Image.asset(
                  'assets/map_layer_icon.png',
                  height: 30,
                  width: 30,
                ),
                itemBuilder: (_) => [
                  new CheckedPopupMenuItem(
                    checked: _selectedView == 'None',
                    value: 'None',
                    child: new Text('None'),
                  ),
                  new CheckedPopupMenuItem(
                    checked: _selectedView == 'Normal',
                    value: 'Normal',
                    child: new Text('Normal'),
                  ),
                  new CheckedPopupMenuItem(
                    checked: _selectedView == 'Hybrid',
                    value: 'Hybrid',
                    child: new Text('Hybrid'),
                  ),
                  new CheckedPopupMenuItem(
                    checked: _selectedView == 'Satellite',
                    value: 'Satellite',
                    child: new Text('Satellite'),
                  ),
                  new CheckedPopupMenuItem(
                    checked: _selectedView == 'Terrain',
                    value: 'Terrain',
                    child: new Text('Terrain'),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4),
              ),
              GestureDetector(
                onTap: () {
                  scaffoldKey.currentState.openEndDrawer();
                },
                child: Image.asset(
                  'assets/vehicle_icon.png',
                  height: 30,
                  width: 30,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4),
              ),
            ],
          ),
          endDrawer: Drawer(
              child: state.departmentList != null
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 130,
                          width: double.infinity,
                          padding: EdgeInsets.all(16),
                          color: Colors.teal,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Vehicle List',
                              style: Theme.of(context).textTheme.title.copyWith(
                                    color: Colors.white,
                                  ),
                            ),
                          ),
                        ),
                        state.departmentList.isNotEmpty
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: state.departmentList.map((item) {
                                  Department department = item;
                                  int departmentIndex =
                                      state.departmentList.indexOf(item);
                                  return department
                                          .departmentVehiclesList.isNotEmpty
                                      ? Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 8),
                                              width: double.infinity,
                                              height: 30,
                                              color: Colors.teal,
                                              child: Text(
                                                department.name,
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                            Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: department
                                                    .departmentVehiclesList
                                                    .map((vehicle) {
                                                  DepartmentVehicles
                                                      departmentVehicles =
                                                      vehicle;
                                                  int vehicleIndex = department
                                                      .departmentVehiclesList
                                                      .indexOf(vehicle);
                                                  return GestureDetector(
                                                    onTap: () {
                                                      // deviceName =
                                                      //     departmentVehicles
                                                      //         .deviceName;

                                                      // Navigator.pop(context);
                                                      // requestCurrentPosition(
                                                      //     departmentVehicles);
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      child: Row(
                                                        children: <Widget>[
                                                          Checkbox(
                                                            value:
                                                                departmentVehicles
                                                                    .isChecked,
                                                            // activeColor:
                                                            //     Colors.grey,
                                                            checkColor:
                                                                Colors.white,
                                                            onChanged: (value) {
                                                              deviceName =
                                                                  departmentVehicles
                                                                      .deviceName;
                                                              Navigator.pop(
                                                                  context);
                                                              updateCheckValues(
                                                                value,
                                                                departmentVehicles,
                                                                departmentIndex,
                                                                vehicleIndex,
                                                              );
                                                            },
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                              departmentVehicles
                                                                  .deviceName,
                                                              style: TextStyle(
                                                                fontSize: 14,
                                                                color: departmentVehicles
                                                                            .event
                                                                            .toLowerCase() ==
                                                                        "deviceonline"
                                                                    ? Colors
                                                                        .green
                                                                    : Colors
                                                                        .red,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                }).toList()),
                                          ],
                                        )
                                      : Container();
                                }).toList())
                            : Container(),
                      ],
                    )
                  : Container()),
          body: Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: GoogleMap(
                onMapCreated: _onMapCreated,
                mapType: mapType,
                initialCameraPosition: cameraPosition,
                markers: Set<Marker>.of(markers.values),
              ),
            ),
          ),
        );
      },
    );
  }
}

class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.white,
  }) : super(listenable: controller);

  final PageController controller;
  final int itemCount;
  final ValueChanged<int> onPageSelected;
  final Color color;
  static const double _kDotSize = 8.0;
  static const double _kMaxZoom = 1.7;
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 0.5 + (_kMaxZoom - 1.0) * selectedness;
    return new Container(
      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: color,
          type: MaterialType.circle,
          child: new Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}
