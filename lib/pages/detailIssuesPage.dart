import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/issues.dart';

class OpenIssuesPage extends StatefulWidget {
  final List<Issues> openIssuesList;
  OpenIssuesPage({this.openIssuesList});
  @override
  _OpenIssuesPageState createState() => _OpenIssuesPageState();
}

class _OpenIssuesPageState extends State<OpenIssuesPage> {
  List<Issues> openIssuesList = new List();

  @override
  void initState() {
    widget.openIssuesList.forEach((item) {
      if (item.status.toLowerCase() == 'open') {
        openIssuesList.add(Issues(
            item.id,
            item.issueNumber,
            item.title,
            item.description,
            item.department,
            item.vehicle,
            item.status,
            item.created,
            item.detailUrl));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        
        return Scaffold(
            body: openIssuesList.isEmpty
                    ? Container(
                        child: Center(
                          child: Text('No Issues'),
                        ),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: openIssuesList.map((item) {
                          Issues issues = item;
                          return GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (context) =>
                              //             DetailIssuesPage()));
                              // scaffoldKey.currentState
                              //     .showBottomSheet((BuildContext context) {
                              //   return Text('This is bottom sheet');
                              // });
                            },
                            child: Card(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 8),
                                width: double.infinity,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        issues.issueNumber,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      issues.title,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    Text(
                                      issues.description,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    Text(
                                      issues.created,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    Text(
                                      issues.status.toUpperCase(),
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }).toList()));
      },
    );
  }
}

class ClosedIssuesPage extends StatefulWidget {
  final List<Issues> closedIssuesList;
  ClosedIssuesPage({this.closedIssuesList});
  @override
  _ClosedIssuesPageState createState() => _ClosedIssuesPageState();
}

class _ClosedIssuesPageState extends State<ClosedIssuesPage> {
  List<Issues> closedIssuesList = new List();

  @override
  void initState() {
    widget.closedIssuesList.forEach((item) {
      if (item.status.toLowerCase() == 'close') {
        closedIssuesList.add(Issues(
            item.id,
            item.issueNumber,
            item.title,
            item.description,
            item.department,
            item.vehicle,
            item.status,
            item.created,
            item.detailUrl));
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
            body: closedIssuesList.isEmpty
                ? Container(
                    child: Center(
                      child: Text('No Issues'),
                    ),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: closedIssuesList.map((item) {
                      Issues issues = item;
                      return GestureDetector(
                        onTap: () {
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             DetailIssuesPage()));
                          // scaffoldKey.currentState
                          //     .showBottomSheet((BuildContext context) {
                          //   return Text('This is bottom sheet');
                          // });
                        },
                        child: Card(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    issues.issueNumber,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                                Text(
                                  issues.title,
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                                Text(
                                  issues.description,
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                                Text(
                                  issues.created,
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                                Text(
                                  issues.status.toUpperCase(),
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }).toList()));
      },
    );
  }
}
