import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/overSpeedReport.dart';
import 'package:gtrack/utils/textUtils.dart';

class OverspeedReportsPage extends StatefulWidget {
  @override
  _OverspeedReportsPageState createState() => _OverspeedReportsPageState();
}

class _OverspeedReportsPageState extends State<OverspeedReportsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Container(
            child: state.overSpeedReportsList == null
                ? Center(
                    child: Container(),
                  )
                : state.overSpeedReportsList.isEmpty
                    ? Center(
                        child: Text('No Report'),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            color: Colors.grey.withOpacity(0.4),
                            child: Table(
                              children: [
                                TableRow(children: [
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 8.0),
                                      child: Text(
                                        'I.TIME',
                                        textAlign: TextAlign.left,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'F.TIME',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'SPEED',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'DIST',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'T.TIME',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                ])
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children:
                                    state.overSpeedReportsList.map((item) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 8, vertical: 8),
                                        width: double.infinity,
                                        color: Colors.grey.withOpacity(0.4),
                                        child: Text(
                                          item.date,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        child: Table(
                                            columnWidths: {
                                              0: FixedColumnWidth(150.0)
                                            },
                                            children:
                                                item.overSpeedList.map((item) {
                                              OverSpeed overspeed = item;

                                              return TableRow(children: [
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      vertical: 8.0,
                                                    ),
                                                    child: Text(
                                                      overspeed.initialTime,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      overspeed.finalTime,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      overspeed.speed
                                                          .toString(),
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      overspeed.totalDistance,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      overspeed.totalTime,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                              ]);
                                            }).toList()),
                                      ),
                                    ],
                                  );
                                }).toList()),
                          ),
                        ],
                      ));
      },
    );
  }
}
