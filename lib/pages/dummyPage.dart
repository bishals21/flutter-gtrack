// import 'dart:async';
// import 'package:dio/dio.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:gtrack/custom_package/linearPercentageIndicator.dart';
// import 'package:gtrack/utils/textUtils.dart';

// class DummyHomePage extends StatefulWidget {
//   @override
//   _DummyHomePageState createState() => _DummyHomePageState();
// }

// class _DummyHomePageState extends State<DummyHomePage> {
//   static String baseUrl = "http://portal.trackomotor.com/api/v1/";
//   static String apiKey = "L7cWaOsSuQ0gfE1BvHEzgTL2Z-qcjfbxBdXoIu3Dg9A";
//   static String accessToken =
//       "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ5YXJzaGEiLCJhdWQiOiJBZG1pbiIsImp0aSI6IjFhY1FSMnNwU0JBR254Z0wxWFllWUEiLCJpYXQiOjE1NDY0OTY4OTcsInVzZXJJZCI6MiwiY29tcGFueSI6IllhcnNoYSIsInVzZXJuYW1lIjoieWFyc2hhLnN0dWRpbyIsImV4cCI6MTU0NzEwMTY5N30.6vOCOIfUGAh8j4jn5tBGmZELblZoRIHefFAZuVlwP3o";
//   GoogleMapController controller;

//   List<LatLng> latLngList;
//   List<VehicleLatLng> rotationList;
//   Timer timer;
//   int markerIndex;
//   int start;
//   double rotation;

//   int len;
//   int interval = 1;
//   bool isPause = false;

//   void _onMapCreated(GoogleMapController controller) {
//     this.controller = controller;
//     controller
//         .animateCamera(CameraUpdate.newLatLng(LatLng(27.700769, 85.300140)));

//     // controller.addMarker(
//     //   // MarkerOptions(
//     //   //   position: LatLng(27.700769, 85.300140),
//     //   //   infoWindowText: InfoWindowText('Starting Position', '*'),
//     //   //   icon: BitmapDescriptor.fromAsset('assets/navigator_icon.png'),
//     //   //   rotation: 270,
//     //   // ),
//     // );
//   }

//   pathHistory() async {

//     Dio dio = new Dio();
//     Response response = await dio.get(
//       "http://portal.trackomotor.com/api/v1/company/33/positions?_format=json&from_date=2019-03-17+09:59:36&to_date=2019-03-18+09:59:36",
//       options: Options(headers: {
//         'api-key': TextUtils.apiKey,
//         'access-token': TextUtils.accessToken,
//       }),
//     );
//     if (response.statusCode == 200) {
//       final positions = response.data['data']['positions'];

//       List<VehicleLatLng> list = new List();
//       List<LatLng> routesList = new List();

//       positions.forEach((item) {
//         list.add(VehicleLatLng(
//           double.parse(item['latitude']),
//           double.parse(item['longitude']),
//           item['course'],
//           item['speed'],
//           item['address'],
//           item['device_time'],
//         ));
//         routesList.add(LatLng(
//             double.parse(item['latitude']), double.parse(item['longitude'])));
//       });

//       //List<VehicleLatLng> routeHistory = new List();

//       // List<LatLng> routesList = new List();
//       // print('RouteList : ${routesList.length}');
//       // jsonResponse.forEach((item) {
//       //   routesList
//       //       .add(LatLng(double.parse(item['lat']), double.parse(item['lng'])));
//       // });
//       latLngList = routesList;
//       rotationList = list;
//       len = latLngList.length;

//       addPolyLines();
//     }
//   }

//   void addPolyLines() {
//     controller.addPolyline(PolylineOptions(
//         points: latLngList, color: Colors.red.value, width: 10, visible: true));

//     setState(() {});
//     addMarker(latLngList, 0);
//   }

//   addMarker(List<LatLng> latLngList, int index) {

//     // if (double.parse(rotationList[index].course) > 180.0) {
//     //   rotation = double.parse(rotationList[index].course) / 2.0;
//     // } else {
//     //   rotation = double.parse(rotationList[index].course);
//     // }

//     controller.addMarker(
//       MarkerOptions(
//         zIndex: 100,
//         position:
//             LatLng(latLngList[index].latitude, latLngList[index].longitude),
//         infoWindowText: InfoWindowText('Starting Position', '*'),
//         icon: BitmapDescriptor.fromAsset('assets/marker_arrow.png'),
//         anchor: Offset(0.5, 0.5),
//         flat: true,
//         infoWindowAnchor: Offset(0.5, 0.0),
//         rotation: double.parse(rotationList[index].course) / 1.0,

//         // (Random().nextInt(270)).toDouble(),
//       ),
//     );

//     setState(() {});
//     controller.animateCamera(CameraUpdate.newLatLngZoom(
//         LatLng(latLngList[index].latitude, latLngList[index].longitude), 16));
//   }

//   // checking rotation

//   void checkRotationWithTimer() {
//     var initialTime = 360;

//     var oneSec = Duration(seconds: 5);
//     var rot = 360.0;
//     timer = new Timer.periodic(
//         oneSec,
//         (Timer timer) => setState(() {
//               if (initialTime < 1) {
//                 timer.cancel();
//               } else {
//                 initialTime = initialTime - 5;
//                 rot = rot + 10;
//                 setState(() {
//                   controller.clearMarkers();
//                   controller.addMarker(MarkerOptions(
//                     position: LatLng(27.700769, 85.300140),
//                     flat: true,
//                     icon: BitmapDescriptor.fromAsset('assets/marker_arrow.png'),
//                     infoWindowAnchor: Offset(0.5, 0.7),
//                     infoWindowText: InfoWindowText(
//                         'Rotation', 'Rotation degree : $initialTime'),
//                     anchor: Offset(0.5, 0.5),
//                     rotation: rot,
//                   ));

//                 });
//               }
//             }));
//   }

//   void startTimer(int s, int index) {

//     var oneSec = Duration(seconds: interval);

//     markerIndex = index;
//     start = s;
//     timer = new Timer.periodic(
//         oneSec,
//         (Timer timer) => setState(() {
//               if (start < 1) {
//                 timer.cancel();
//               } else {
//                 start = start - 1;
//                 controller.clearMarkers();
//                 addMarker(latLngList, markerIndex);
//                 setState(() {
//                   isPause = true;
//                   markerIndex++;
//                   len = start;
//                 });

//               }
//             }));
//   }

//   pauseNavigation() {
//     setState(() {
//       timer.cancel();
//       isPause = false;
//     });

//   }

//   void _select(Choice choice) {
//     setState(() {
//       if (timer != null) {
//         timer.cancel();
//         interval = int.parse(choice.title);
//         startTimer(start, markerIndex);
//       } else {
//         interval = int.parse(choice.title);
//         startTimer(len == latLngList.length ? latLngList.length : start,
//             markerIndex == null ? 0 : markerIndex);
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('G-Track'),
//         actions: <Widget>[
//           latLngList != null
//               ? PopupMenuButton<Choice>(
//                   onSelected: _select,
//                   itemBuilder: (BuildContext context) {
//                     return choices.map((Choice choice) {
//                       return PopupMenuItem<Choice>(
//                         value: choice,
//                         child: Text(choice.title),
//                       );
//                     }).toList();
//                   },
//                 )
//               : Container()
//         ],
//       ),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: <Widget>[
//           Center(
//             child: SizedBox(
//               width: double.infinity,
//               height: 450.0,
//               child: GoogleMap(
//                 onMapCreated: _onMapCreated,
//                 initialCameraPosition: const CameraPosition(
//                   target: LatLng(27.700769, 85.300140),
//                   zoom: 15.0,
//                 ),
//                 gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
//                   Factory<OneSequenceGestureRecognizer>(
//                     () => EagerGestureRecognizer(),
//                   ),
//                 ].toSet(),
//               ),
//             ),
//           ),
//           Expanded(
//               child: SingleChildScrollView(
//             child: Row(
//               children: <Widget>[
//                 FlatButton(
//                   child: const Text('Get Path History'),
//                   onPressed: () {
//                     //  addPolyLines();
//                     pathHistory();
//                   },
//                 ),
//                 FlatButton(
//                   child: Icon(isPause ? Icons.pause : Icons.play_arrow),
//                   onPressed: () {
//                     isPause
//                         ? pauseNavigation()
//                         : startTimer(
//                             len == latLngList.length
//                                 ? latLngList.length
//                                 : start,
//                             markerIndex == null ? 0 : markerIndex);
//                   },
//                 ),
//                 // Container(
//                 //   height: 30,
//                 //   width: 30,
//                 //   decoration: BoxDecoration(
//                 //       image: DecorationImage(
//                 //           image: AssetImage('assets/navigator_icon.png'))),
//                 // )
//               ],
//             ),
//           ))
//         ],
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () => checkRotationWithTimer(),
//         child: Text('Rotate'),
//       ),
//     );
//   }
// }

// class Choice {
//   const Choice({
//     this.title,
//   });

//   final String title;
// }

// const List<Choice> choices = const <Choice>[
//   const Choice(
//     title: '1',
//   ),
//   const Choice(
//     title: '2',
//   ),
//   const Choice(
//     title: '3',
//   ),
//   const Choice(
//     title: '4',
//   ),
// ];

// class PercentageIndicatorPage extends StatefulWidget {
//   @override
//   _PercentageIndicatorPageState createState() =>
//       _PercentageIndicatorPageState();
// }

// class _PercentageIndicatorPageState extends State<PercentageIndicatorPage> {
//   double total = 1.0;
//   double initialValue = 0.0;
//   Timer timer;
//   double percentageValue = 0.0;
//   int start = 9;

//   calculatePercentage() {
//     setState(() {
//       percentageValue = (initialValue / total) * 100;
//     });
//   }

//   void startTimer() {
//     var oneSec = Duration(seconds: 1);

//     timer = new Timer.periodic(
//         oneSec,
//         (Timer timer) => setState(() {
//               if (start < 1) {
//                 timer.cancel();
//               } else {
//                 start = start - 1;
//                 initialValue = initialValue + 0.1;
//                 calculatePercentage();
//               }
//             }));
//   }

//   @override
//   void initState() {
//     startTimer();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Percentage Indicator'),
//       ),
//       body: Center(
//         child: Padding(
//           padding: EdgeInsets.all(15.0),
//           child: new LinearPercentIndicator(
//             width: MediaQuery.of(context).size.width - 50,
//             animation: true,
//             lineHeight: 20.0,
//             animationDuration: 2000,
//             percent: percentageValue,
//             animateFromLastPercent: true,
//             center: Text(" ${percentageValue.toStringAsFixed(2)}"),
//             linearStrokeCap: LinearStrokeCap.roundAll,
//             progressColor: Colors.teal,
//           ),
//         ),
//       ),
//     );
//   }
// }

// class MarkerDemo extends StatefulWidget {
//   @override
//   _MarkerDemoState createState() => _MarkerDemoState();
// }

// class _MarkerDemoState extends State<MarkerDemo> {
//   GoogleMapController controller;
//   Marker marker;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//         child: SizedBox(
//           width: double.infinity,
//           height: double.infinity,
//           child: GoogleMap(
//             onMapCreated: _onMapCreated,
//             initialCameraPosition: const CameraPosition(
//               target: LatLng(27.700769, 85.300140),
//               zoom: 15.0,
//             ),
//             gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
//               Factory<OneSequenceGestureRecognizer>(
//                 () => EagerGestureRecognizer(),
//               ),
//             ].toSet(),
//           ),
//         ),
//       ),
//     );
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     this.controller = controller;
//     controller
//         .animateCamera(CameraUpdate.newLatLng(LatLng(27.700769, 85.300140)));
//   }
// }

// class VehicleLatLng {
//   double latitude;
//   double longitude;
//   String course;
//   String speed;
//   String address;
//   String deviceTime;

//   VehicleLatLng(this.latitude, this.longitude, this.course, this.speed,
//       this.address, this.deviceTime);
// }

import 'package:flutter/material.dart';

class DummyPage extends StatefulWidget {
  @override
  _DummyPageState createState() => _DummyPageState();
}

class _DummyPageState extends State<DummyPage> {
  @override
  void initState() {
    super.initState();
    var now = DateTime.now();
    print("Now : $now");
    String datetime = now.toString();
    var time = DateTime.parse(datetime);
    print("Time : $time");
    Future.delayed(Duration(seconds: 65), () {
      var result = DateTime.now().difference(now);

      print("then : ${DateTime.now()}");
      print("Result is : ${result.inMinutes}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dummy Pages"),
      ),
    );
  }
}
