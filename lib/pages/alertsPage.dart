import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/actions/alertsAction.dart';
import 'package:gtrack/actions/disposeActions.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/pages/alertsDetailPage.dart';

class AlertsPage extends StatefulWidget {
  @override
  _AlertsPageState createState() => _AlertsPageState();
}

class _AlertsPageState extends State<AlertsPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      onInit: (store) {
        store.dispatch(EventAlertsAction().eventAlerts(scaffoldKey));
        store
            .dispatch(MaintenanceAlertsAction().maintenanceAlerts(scaffoldKey));
      },
      onDispose: (store){
        store.dispatch(DisposeActions.DisposeEventAlerts);
        store.dispatch(DisposeActions.DisposeMaintenanceAlertsList);
      },
      converter: (store) => store.state,
      builder: (_, state) {
        return DefaultTabController(
          length: 2,
          child: Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
              elevation: 0.0,
              title: Text('Alerts Page'),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  color: Colors.teal,
                  child: TabBar(
                    indicatorColor: Colors.white,
                    tabs: <Widget>[
                      Tab(
                        text: 'Events Alerts',
                      ),
                      Tab(text: 'Maintenance Alerts'),
                    ],
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: <Widget>[
                      EventAlertsPage(),
                      MaintenanceAlertsPage(),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
