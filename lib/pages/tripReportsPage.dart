import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/tripReport.dart';
import 'package:gtrack/utils/textUtils.dart';

class TripReportsPage extends StatefulWidget {
  // final List<Trips> tripReportsList;
  // TripReportsPage({this.tripReportsList});

  @override
  _TripReportsPageState createState() => _TripReportsPageState();
}

class _TripReportsPageState extends State<TripReportsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (_, state) {
          return Container(
            child: state.tripReportsList == null
                ? Center(
                    child: Container(),
                  )
                : state.tripReportsList.isEmpty
                    ? Center(
                        child: Text('No Report '),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            color: Colors.grey.withOpacity(0.4),
                            child: Table(
                              columnWidths: {
                                1: FixedColumnWidth(80.0),
                                3: FixedColumnWidth(80.0),
                              },
                              children: [
                                TableRow(children: [
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 8.0),
                                      child: Text(
                                        'I. TIME',
                                        textAlign: TextAlign.left,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'LOC',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'F. TIME',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'LOC',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'DIST',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        'T. TIME',
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                ])
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: state.tripReportsList.map((item) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 8, vertical: 8),
                                        width: double.infinity,
                                        color: Colors.grey.withOpacity(0.4),
                                        child: Text(
                                          item.date,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        child: Table(
                                            columnWidths: {
                                              1: FixedColumnWidth(80.0),
                                              3: FixedColumnWidth(80.0),
                                            },
                                            children:
                                                item.tripsList.map((item) {
                                              Trips trips = item;

                                              return TableRow(children: [
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      vertical: 8.0,
                                                    ),
                                                    child: Text(
                                                      trips.initialTime,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      trips.initialLocation,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      trips.finalTime,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      trips.finalLocation,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      trips.distance,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                                TableCell(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 8.0),
                                                    child: Text(
                                                      trips.finalTime,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style:
                                                          TextUtils.titleStyle,
                                                    ),
                                                  ),
                                                ),
                                              ]);
                                            }).toList()),
                                      ),
                                    ],
                                  );
                                }).toList()),
                          ),
                          // Container(
                          //   child: Table(
                          //     columnWidths: {
                          //       0: FixedColumnWidth(MediaQuery.of(context).size.width),
                          //     },
                          //     children: [
                          //       TableRow(children: [
                          //         TableCell(
                          //           child: Padding(
                          //             padding: const EdgeInsets.symmetric(
                          //                 horizontal: 8, vertical: 8.0),
                          //             child: Text(
                          //               '2019-03-27',
                          //               textAlign: TextAlign.left,
                          //               style: TextUtils.titleStyle,
                          //             ),
                          //           ),
                          //         ),
                          //         TableCell(
                          //           child: Padding(
                          //             padding: const EdgeInsets.symmetric(vertical: 8.0),
                          //             child: Text(
                          //               '',
                          //               textAlign: TextAlign.center,
                          //               style: TextUtils.titleStyle,
                          //             ),
                          //           ),
                          //         ),
                          //         TableCell(
                          //           child: Padding(
                          //             padding: const EdgeInsets.symmetric(vertical: 8.0),
                          //             child: Text(
                          //               '',
                          //               textAlign: TextAlign.center,
                          //               style: TextUtils.titleStyle,
                          //             ),
                          //           ),
                          //         ),
                          //         TableCell(
                          //           child: Padding(
                          //             padding: const EdgeInsets.symmetric(vertical: 8.0),
                          //             child: Text(
                          //               '',
                          //               textAlign: TextAlign.center,
                          //               style: TextUtils.titleStyle,
                          //             ),
                          //           ),
                          //         ),
                          //         TableCell(
                          //           child: Padding(
                          //             padding: const EdgeInsets.symmetric(vertical: 8.0),
                          //             child: Text(
                          //               '',
                          //               textAlign: TextAlign.center,
                          //               style: TextUtils.titleStyle,
                          //             ),
                          //           ),
                          //         ),
                          //         TableCell(
                          //           child: Padding(
                          //             padding: const EdgeInsets.symmetric(vertical: 8.0),
                          //             child: Text(
                          //               '',
                          //               textAlign: TextAlign.center,
                          //               style: TextUtils.titleStyle,
                          //             ),
                          //           ),
                          //         ),
                          //       ])
                          //     ],
                          //   ),
                          // ),
                        ],
                      ),
          );
        });
  }
}
