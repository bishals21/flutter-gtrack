import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/issues.dart';

class CompanyIssuesPage extends StatefulWidget {
  @override
  _CompanyIssuesPageState createState() => _CompanyIssuesPageState();
}

class _CompanyIssuesPageState extends State<CompanyIssuesPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      // onInit: (store) => store.dispatch(IssuesAction().issues(scaffoldKey)),
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
              title: Text('Company Issue'),
              backgroundColor: Colors.teal,
            ),
            body: state.issue == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : state.issue.issuesList.isEmpty
                    ? Center(
                        child: Text('No Issues Found'),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: state.issue.issuesList.map((item) {
                          Issues issues = item;
                          return GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (context) =>
                              //             DetailIssuesPage()));
                              scaffoldKey.currentState
                                  .showBottomSheet((BuildContext context) {
                                return Text('This is bottom sheet');
                              });
                            },
                            child: Card(
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 8),
                                width: double.infinity,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        issues.title.toUpperCase(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      issues.department,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    Text(
                                      issues.issueNumber,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    Text(
                                      issues.created,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                      ));
      },
    );
  }
}

class DetailIssuesPage extends StatefulWidget {
  @override
  _DetailIssuesPageState createState() => _DetailIssuesPageState();
}

class _DetailIssuesPageState extends State<DetailIssuesPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Scaffold(
          key: scaffoldKey,
          body: Center(child: Text('Detail Issues')),
        );
      },
    );
  }
}
