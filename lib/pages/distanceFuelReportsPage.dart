import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/distanceFuelReport.dart';
import 'package:gtrack/utils/textUtils.dart';

class DistanceFuelReportsPage extends StatefulWidget {
  // final List<DistanceFuelReport> distanceFuelReportList;
  // DistanceFuelReportsPage({this.distanceFuelReportList});

  @override
  _DistanceFuelReportsPageState createState() =>
      _DistanceFuelReportsPageState();
}

class _DistanceFuelReportsPageState extends State<DistanceFuelReportsPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Container(
          child: state.distanceFuelReportsList == null
              ? Center(child: Container())
              : state.distanceFuelReportsList.isEmpty
                  ? Center(
                      child: Text('No Reports'),
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          color: Colors.grey.withOpacity(0.4),
                          child: Table(
                            columnWidths: {0: FixedColumnWidth(120.0)},
                            children: [
                              TableRow(children: [
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8.0),
                                    child: Text(
                                      'TARGET NAME',
                                      textAlign: TextAlign.left,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'V.TYPE',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'MILEAGE (km/l)',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'DIST (km)',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Text(
                                      'FUEL CONSM',
                                      textAlign: TextAlign.center,
                                      style: TextUtils.titleStyle,
                                    ),
                                  ),
                                ),
                              ])
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          child: Table(
                              columnWidths: {0: FixedColumnWidth(120.0)},
                              children:
                                  state.distanceFuelReportsList.map((item) {
                                DistanceFuelReport distanceFuelReport = item;

                                return TableRow(children: [
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 8.0,
                                      ),
                                      child: Text(
                                        distanceFuelReport.targetName,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        distanceFuelReport.vehicleType,
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        distanceFuelReport.mileage,
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        distanceFuelReport.totalDistance,
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                  TableCell(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Text(
                                        distanceFuelReport.fuelConsumed,
                                        textAlign: TextAlign.center,
                                        style: TextUtils.titleStyle,
                                      ),
                                    ),
                                  ),
                                ]);
                              }).toList()),
                        ),
                      ],
                    ),
        );
      },
    );
  }
}
