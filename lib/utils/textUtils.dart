import 'package:flutter/material.dart';

class TextUtils {
  static final navigatorKey = GlobalKey<NavigatorState>();
 

  static String apiKey = 'L7cWaOsSuQ0gfE1BvHEzgTL2Z-qcjfbxBdXoIu3Dg9A';
  static String baseUrl = 'http://portal.trackomotor.com/api/v1/';

  static String loginUrl = baseUrl + 'authenticate';

  static String companyDepartmentsUrl = baseUrl + 'company/departments';

  static String distanceFuelReportUrl =
      baseUrl + 'company/distance-fuel-report';
  static String eventAlertsUrl = baseUrl + 'company/event-alerts';
  static String issuesUrl = baseUrl + 'company/issues';
  static String companyIssuesUrl = baseUrl + 'company/issue';
  static String maintenanceAlertsUrl = baseUrl + 'company/maintenance-alerts';
  static String overallReportUrl = baseUrl + 'company/overall-report';

  // trail access-token
  static String accessToken =
      'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ5YXJzaGEiLCJhdWQiOiJBZG1pbiIsImp0aSI6IjFhY1FSMnNwU0JBR254Z0wxWFllWUEiLCJpYXQiOjE1NDY0OTY4OTcsInVzZXJJZCI6MiwiY29tcGFueSI6IllhcnNoYSIsInVzZXJuYW1lIjoieWFyc2hhLnN0dWRpbyIsImV4cCI6MTU0NzEwMTY5N30.6vOCOIfUGAh8j4jn5tBGmZELblZoRIHefFAZuVlwP3o';

  static final titleStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 12);


      static final String webSocketUrl = "ws://103.198.9.214:8080/gps-server/api/socket/";
}
