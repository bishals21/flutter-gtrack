import 'package:flutter/material.dart';

class DialoguesUtils {
  static final scaffoldKey = GlobalKey<ScaffoldState>();

  static showSnackBar(GlobalKey<ScaffoldState> key, String text) {
    key.currentState.showSnackBar(
      SnackBar(
        content: Text('$text'),
        backgroundColor: Colors.teal,
      ),
    );
  }

  static showProgressDialogue(BuildContext context, String text) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircularProgressIndicator(
                  valueColor:
                      new AlwaysStoppedAnimation(const Color(0xFF26b8eb)),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                ),
                Text('$text...')
              ],
            ),
          ),
        );
      },
    );
  }
}
