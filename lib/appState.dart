import 'dart:convert';
import 'package:gtrack/model/department.dart';
import 'package:gtrack/model/distanceFuelReport.dart';
import 'package:gtrack/model/eventAlerts.dart';
import 'package:gtrack/model/issues.dart';
import 'package:gtrack/model/maintenanceAlerts.dart';
import 'package:gtrack/model/overSpeedReport.dart';
import 'package:gtrack/model/overallReports.dart';
import 'package:gtrack/model/servicingReports.dart';
import 'package:gtrack/model/tripReport.dart';
import 'package:gtrack/model/user.dart';
import 'package:gtrack/model/vehicle.dart';

class AppState {
  User user;
  List<VehiclePosition> routeHistoryList;
  List<Department> departmentList;
  Issue issue;
  CompanyIssue companyIssue;
  List<MaintenanceAlerts> maintenanceAlertsList;
  EventAlerts eventAlerts;
  List<OverallReports> overallReportsList;
  List<DistanceFuelReport> distanceFuelReportsList;
  List<TripReports> tripReportsList;
  List<OverSpeedReports> overSpeedReportsList;
  List<ServicingReports> servicingReportsList;

  AppState({
    this.user,
    this.routeHistoryList,
    this.departmentList,
    this.issue,
    this.companyIssue,
    this.maintenanceAlertsList,
    this.eventAlerts,
    this.overallReportsList,
    this.distanceFuelReportsList,
    this.tripReportsList,
    this.overSpeedReportsList,
    this.servicingReportsList,
  });

  AppState copyWith({
    Nullable<User> user,
    List<VehiclePosition> routeHistoryList,
    List<Department> departmentList,
    Issue issue,
    CompanyIssue companyIssue,
    List<MaintenanceAlerts> maintenanceAlertsList,
    EventAlerts eventAlerts,
    List<OverallReports> overallReportsList,
    List<DistanceFuelReport> distanceFuelReportList,
    List<TripReports> tripReportsList,
    List<OverSpeedReports> overSpeedReportsList,
    List<ServicingReports> servicingReportsList,
  }) {
    return AppState(
      user: user == null ? this.user : user.value,
      routeHistoryList: routeHistoryList,
      departmentList: departmentList ?? this.departmentList,
      issue: issue ?? this.issue,
      companyIssue: companyIssue ?? this.companyIssue,
      maintenanceAlertsList: maintenanceAlertsList,
      eventAlerts: eventAlerts ?? this.eventAlerts,
      overallReportsList: overallReportsList,
      distanceFuelReportsList: distanceFuelReportList,
      tripReportsList: tripReportsList,
      overSpeedReportsList: overSpeedReportsList,
      servicingReportsList: servicingReportsList,
    );
  }

  static AppState fromJson(dynamic jsonn) {
    if (jsonn != null) {
      String jsonString = jsonn['user'];

      if (jsonString != 'null') {
        var map = json.decode(jsonString);
        User user = User.fromJson(map);

        return AppState(
          user: user,
          routeHistoryList: null,
          departmentList: [],
          issue: null,
          companyIssue: null,
          maintenanceAlertsList: null,
          eventAlerts: null,
          overallReportsList: null,
          distanceFuelReportsList: null,
          tripReportsList: null,
          overSpeedReportsList: null,
          servicingReportsList: null,
        );
      } else {
        return AppState(
          user: null,
          routeHistoryList: null,
          departmentList: [],
          issue: null,
          companyIssue: null,
          maintenanceAlertsList: null,
          eventAlerts: null,
          overallReportsList: null,
          distanceFuelReportsList: null,
          tripReportsList: null,
          overSpeedReportsList: null,
          servicingReportsList: null,
        );
      }
    } else {
      return AppState(
        user: null,
        routeHistoryList: null,
        departmentList: [],
        issue: null,
        companyIssue: null,
        maintenanceAlertsList: null,
        eventAlerts: null,
        overallReportsList: null,
        distanceFuelReportsList: null,
        tripReportsList: null,
        overSpeedReportsList: null,
        servicingReportsList: null,
      );
    }
  }

  dynamic toJson() => {'user': json.encode(user)};
}

class Nullable<T> {
  T _value;

  Nullable(this._value);

  T get value {
    return _value;
  }
}
