class OverallReports {
  String name;
  String mileage;
  String driver;
  String totalDistance;
  String finalDistance;
  String speedCount;
  String speedSum;
  String speedMax;
  String lastSpeed;
  String overSpeedCount;
  bool isInOverSpeed;
  String overspeedLimit;
  String initailDistance;
  int deviceId;
  String fuelConsumption;

  OverallReports(
    this.name,
    this.mileage,
    this.driver,
    this.totalDistance,
    this.finalDistance,
    this.speedCount,
    this.speedSum,
    this.speedMax,
    this.lastSpeed,
    this.overSpeedCount,
    this.isInOverSpeed,
    this.overspeedLimit,
    this.initailDistance,
    this.deviceId,
    this.fuelConsumption,
  );
}
