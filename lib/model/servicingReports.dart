class ServicingReports {
  int id;
  String vehicleName;
  String price;
  String billNumber;
  String remarks;
  String distance;
  String type;
  String date;

  ServicingReports(
    this.id,
    this.vehicleName,
    this.price,
    this.billNumber,
    this.remarks,
    this.distance,
    this.type,
    this.date,
  );
}

// [{id: 3, price: 555.00, billNumber: 123, remarks: ok, distance: 555, type: servicing, date: 2019-02-10 07:10:08}, {id: 4, price: 555.00, billNumber: 123, remarks: oooo, distance: 555, type: maintenance, date: 2019-02-10 07:11:20}]
