class VehicleCurrentPosition {
  String deviceTime;
  String fixTime;
  int altitude;
  int speed;
  int course;
  double latitude;
  double longitude;
  String address;
  String protocol;
   int id;
  String deviceUniqueId;
 
  bool valid;
  
  String event;

  VehicleCurrentPosition(
    this.deviceTime,
    this.fixTime,
    this.altitude,
    this.speed,
    this.course,
    this.latitude,
    this.longitude,
    this.address,
    this.protocol,
    this.id,
    this.deviceUniqueId,
    
    this.valid,
    this.event,
  );
}
