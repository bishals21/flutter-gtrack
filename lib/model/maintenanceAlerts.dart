 
class MaintenanceAlerts {
  int id;
  String name;
  String message;
  bool expiredAlert;

  MaintenanceAlerts(this.id, this.name, this.message, this.expiredAlert);
}
