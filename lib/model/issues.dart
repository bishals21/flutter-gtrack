// list of issues
class Issue {
  List<Issues> issuesList;
  String viewAllLink;
  int currentPage;
  int nextPage;
  int previousPage;
  int total;

  Issue(
    this.issuesList,
    this.viewAllLink,
    this.currentPage,
    this.nextPage,
    this.previousPage,
    this.total,
  );
}

class Issues {
  int id;
  String issueNumber;
  String title;
  String description;
  String department;
  String vehicle;
  String status;
  String created;
  String detailUrl;
  Issues(
    this.id,
    this.issueNumber,
    this.title,
    this.description,
    this.department,
    this.vehicle,
    this.status,
    this.created,
    this.detailUrl,
  );
}

// particular issues

class CompanyIssue {
  Issues issues;
  List<Comments> issuesCommentsList;

  CompanyIssue(this.issues, this.issuesCommentsList);
}

class Comments {
  int id;
  String comment;
  String created;
  String commentator;

  Comments(this.id, this.comment, this.created, this.commentator);
}
