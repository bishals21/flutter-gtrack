class OverSpeedReports {
  String date;
  List<OverSpeed> overSpeedList;

  OverSpeedReports(this.date, this.overSpeedList);
}

class OverSpeed {
  int speed;
  String initialTime;
  String finalTime;
  String initialLocation;
  String totalDistance;
  String totalTime;

  OverSpeed(
    this.speed,
    this.initialTime,
    this.finalTime,
    this.initialLocation,
    this.totalDistance,
    this.totalTime,
  );
}
