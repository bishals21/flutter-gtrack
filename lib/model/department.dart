class Department {
  String name;
  List<DepartmentVehicles> departmentVehiclesList;

  Department(
    this.name,
    this.departmentVehiclesList,
  );
}

class DepartmentVehicles {
  int id;
  String number;
  String code;
  int deviceId;
  String deviceUniqueId;
  String deviceName;
  String deviceStatus;
  String vehicleType;
  String vehicleTypeImage;
  String overSpeedLimit;
  String lastUpdate;
  String longitude;
  String latitude;
  String address;
  String speed;
  String ignition;
  String charge;
  String battery;
  String rssi;
  String course;
  String alarm;
  String lastPositionTime;
  String event;
  String lastEventTime;
  bool isChecked;

  DepartmentVehicles(
    this.id,
    this.number,
    this.code,
    this.deviceId,
    this.deviceUniqueId,
    this.deviceName,
    this.deviceStatus,
    this.vehicleType,
    this.vehicleTypeImage,
    this.overSpeedLimit,
    this.lastUpdate,
    this.longitude,
    this.latitude,
    this.address,
    this.speed,
    this.ignition,
    this.charge,
    this.battery,
    this.rssi,
    this.course,
    this.alarm,
    this.lastPositionTime,
    this.event,
    this.lastEventTime,
    this.isChecked,
  );
}
