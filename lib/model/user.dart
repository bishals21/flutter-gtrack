class User {
  String token;
  String webSocketUrl;
  String role;
  String name;
  String username;
  String contact;
  String company;
  String address;
  String image;

  User({
    this.token,
    this.webSocketUrl,
    this.role,
    this.name,
    this.username,
    this.contact,
    this.company,
    this.address,
    this.image,
  });

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map['token'] = token;
    map['web_socket_url'] = webSocketUrl;
    map['role'] = role;
    map['name'] = name;
    map['username'] = username;
    map['contact'] = contact;
    map['company'] = company;
    map['address'] = address;
    map['image'] = image;
    return map;
  }
 

  User.fromJson(Map<String, dynamic> map)
      : this(
          token: map['token'],
          webSocketUrl: map['web_socket_url'],
          role: map['role'],
          name: map['name'],
          username: map['username'],
          contact: map['contact'],
          company: map['company'],
          address: map['address'],
          image: map['image'],
        );
}
