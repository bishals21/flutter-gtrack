class VehiclePosition {
  bool overSpeed;
  String latitude;
  String longitude;
  String course;
  String speed;
  String address;
  String deviceTime;

  VehiclePosition(this.overSpeed, this.latitude, this.longitude, this.course,
      this.speed, this.address, this.deviceTime);
}
