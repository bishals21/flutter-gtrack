// event alerts
class EventAlerts {
  List<Alert> eventAlertsList;
  String viewAllLink;
  int currentPage;
  int nextPage;
  int previousPage;
  int total;
  EventAlerts(this.eventAlertsList, this.viewAllLink, this.currentPage,
      this.nextPage, this.previousPage, this.total);
}

class Alert {
  String location;
  String latitude;
  String longitude;
  String time;
  String vehicle;
  String type;

  Alert(this.location, this.latitude, this.longitude, this.time, this.vehicle,
      this.type);
}
