class DistanceFuelReport {
  String targetName;
  String totalDistance;
  String averageSpeed;
  String maxSpeed;
  String fuelConsumed;
  int deviceId;
  String mileage;
  String vehicleType;

  DistanceFuelReport(
    this.targetName,
    this.totalDistance,
    this.averageSpeed,
    this.maxSpeed,
    this.fuelConsumed,
    this.deviceId,
    this.mileage,
    this.vehicleType,
  );
}
