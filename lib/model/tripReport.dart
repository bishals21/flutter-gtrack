class TripReports {
  String date;
  List<Trips> tripsList;

  TripReports(this.date, this.tripsList);
}

class Trips {
  String initialTime;
  String finalTime;
  String totalTime;
  String distance;
  String initialLocation;
  String finalLocation;

  Trips(this.initialTime, this.finalTime,this.totalTime, this.distance, this.initialLocation,
      this.finalLocation);
}
