class CurrentVehicle {
  int id;
  String vehicleName;
  String previousTime;

  CurrentVehicle(
    this.id,
    this.vehicleName,
    this.previousTime,
  );

  // convert to map
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = id;
    map['vehicleName'] = vehicleName;
    map['previousTime'] = previousTime;

    return map;
  }

  // convert to map
  CurrentVehicle.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.vehicleName = map['vehicleName'];
    this.previousTime = map['previousTime'];
  }
}
