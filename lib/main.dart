import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gtrack/appReducer.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/pages/homePage.dart';
import 'package:gtrack/pages/loginPage.dart';
import 'package:gtrack/utils/textUtils.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:redux_thunk/redux_thunk.dart';

void main() async {
  final persistor = Persistor<AppState>(
    storage: FlutterStorage(),
    serializer: JsonSerializer<AppState>(AppState.fromJson),
  );
  final initialState = await persistor.load();
  var store = Store<AppState>(appReducer,
      initialState: initialState,
      middleware: [thunkMiddleware, persistor.createMiddleware()]);
  runApp(MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;
  MyApp(this.store);
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        navigatorKey: TextUtils.navigatorKey,
        debugShowCheckedModeBanner: false,
        title: 'G-Track',
        theme: ThemeData(primarySwatch: Colors.teal, cursorColor: Colors.teal),
        home: store.state.user != null ? HomePage() : LoginPage(),
      ),
    );
  }
}
