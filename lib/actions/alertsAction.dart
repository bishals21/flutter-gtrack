import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/eventAlerts.dart';
import 'package:gtrack/model/maintenanceAlerts.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:redux/redux.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:gtrack/utils/internetConnectionUtils.dart';
import 'package:gtrack/utils/textUtils.dart';

class MaintenanceAlertsAction {
  ThunkAction<AppState> maintenanceAlerts(GlobalKey<ScaffoldState> key) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio
            .get(TextUtils.maintenanceAlertsUrl,
                options: Options(headers: {
                  'api-key': TextUtils.apiKey,
                  'access-token': store.state.user.token,
                }))
            .then((response) {
          final data = response.data['data'];
          final alerts = data['alerts'];
        
          List<MaintenanceAlerts> maintenanceAlertsList = new List();
          alerts.forEach((item) {
            maintenanceAlertsList.add(MaintenanceAlerts(
              item['id'],
              item['name'],
              item['message'],
              item['expiredAlert'],
            ));
          });

          store.dispatch(MaintenanceAlertsFetched(
              maintenanceAlertsList: maintenanceAlertsList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class MaintenanceAlertsFetched {
  List<MaintenanceAlerts> maintenanceAlertsList;
  MaintenanceAlertsFetched({this.maintenanceAlertsList});
}

class EventAlertsAction {
  ThunkAction<AppState> eventAlerts(GlobalKey<ScaffoldState> key) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        store.dispatch(IsLoadingAction(isLoading: true));
        Dio dio = Dio();
        dio.get(TextUtils.eventAlertsUrl,
            options: Options(headers: {
              'api-key': TextUtils.apiKey,
              'access-token': store.state.user.token,
            }),
            queryParameters: {'page': '1'}).then((response) {
          final data = response.data['data'];
          final alerts = data['alerts'];
          List<Alert> eventAlertsList = new List();
          alerts.forEach((item) {
            eventAlertsList.add(
              Alert(
                item['location'] ?? '',
                item['latitude'],
                item['longitude'],
                item['time'],
                item['vehicle'],
                item['type'],
              ),
            );
          });
          EventAlerts eventAlerts = new EventAlerts(
            eventAlertsList,
            data['viewAllLink'],
            data['currentPage'],
            data['nextPage'],
            data['previousPage'],
            data['total'],
          );
          store.dispatch(IsLoadingAction(isLoading: false));
          store.dispatch(EventAlertsFetched(eventAlerts: eventAlerts));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class EventAlertsFetched {
  EventAlerts eventAlerts;
  EventAlertsFetched({this.eventAlerts});
}

class LoadMoreAction {
  ThunkAction<AppState> loadMoreEvents(
      GlobalKey<ScaffoldState> key, String page) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio.get(
          TextUtils.eventAlertsUrl,
          options: Options(
            headers: {
              'api-key': TextUtils.apiKey,
              'access-token': store.state.user.token,
            },
          ),
          queryParameters: {
            'page': page,
          },
        ).then((response) {
          final data = response.data['data'];
          final alerts = data['alerts'];
          
          List<Alert> eventAlertsList = store.state.eventAlerts.eventAlertsList;
          alerts.forEach((item) {
            eventAlertsList.add(
              Alert(
                item['location'] ?? '',
                item['latitude'],
                item['longitude'],
                item['time'],
                item['vehicle'],
                item['type'],
              ),
            );
          });
          store.dispatch(EventAlertsFetched(eventAlerts: null));
          EventAlerts eventAlerts = new EventAlerts(
            eventAlertsList,
            data['viewAllLink'],
            data['currentPage'],
            data['nextPage'],
            data['previousPage'],
            data['total'],
          );

          store.dispatch(EventAlertsFetched(eventAlerts: eventAlerts));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Ineternet Connection');
      }
    };
  }
}

class IsLoadingAction {
  bool isLoading;
  IsLoadingAction({this.isLoading});
}
