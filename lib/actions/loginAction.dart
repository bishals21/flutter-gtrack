import 'package:flutter/material.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/user.dart';

import 'package:gtrack/pages/homePage.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:gtrack/utils/internetConnectionUtils.dart';
import 'package:gtrack/utils/textUtils.dart';

import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:dio/dio.dart';

class LoginAction {
  ThunkAction<AppState> userLogin(
      GlobalKey<ScaffoldState> key, String username, String password) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      print("Reachd Connectivity : $isConnected");
      if (isConnected) {
        Dio dio = Dio();
        await dio.post(
          TextUtils.loginUrl,
          options: Options(
            headers: {
              'api-key': TextUtils.apiKey,
            },
          ),
          data: {
            'username': username,
            'password': password,
          },
        ).then((response) {
          final jsonResponse = response.data['data'];
          print("Login response : $jsonResponse");

          final userInfo = jsonResponse['userInfo'];

          TextUtils.navigatorKey.currentState.pop();
          store.dispatch(UserLoggedIn(
              user: User(
            token: jsonResponse['token'] ?? '',
            webSocketUrl: jsonResponse['web_socket_url'] ?? '',
            role: jsonResponse['role'] ?? '',
            name: userInfo['name'] ?? '',
            username: userInfo['username'] ?? '',
            contact: userInfo['contact'] ?? '',
            company: userInfo['company'] ?? '',
            address: userInfo['address'] ?? '',
            image: userInfo['image'] ?? '',
          )));
          store.dispatch(navigateToHomePage(key));
        }).catchError((error) {
          TextUtils.navigatorKey.currentState.pop();
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        TextUtils.navigatorKey.currentState.pop();

        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class UserLoggedIn {
  User user;
  UserLoggedIn({this.user});
}

ThunkAction<AppState> navigateToHomePage(GlobalKey<ScaffoldState> key) {
  return (Store<AppState> store) async {
    TextUtils.navigatorKey.currentState
        .pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
  };
}

class LogoutAction {
  ThunkAction<AppState> logoutUser() {
    return (Store<AppState> store) {
      store.dispatch(Logout.LogoutUser);
    };
  }
}

enum Logout { LogoutUser }
