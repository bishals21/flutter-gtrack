enum DisposeActions {
  LogoutUser,
  DisposeRouteHistoryList,
  DisposeDepartmentList,
  DisposeIssue,
  DisposeCompanyIssue,
  DisposeMaintenanceAlertsList,
  DisposeEventAlerts,
  DisposeOverallReportsList,
  DisposeDistanceFuelReportsList,
  DisposeTripReportsList,
  DisposeOverSpeedReportsList,
  DisposeServicingReportsList,
}
 