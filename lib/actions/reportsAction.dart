import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/distanceFuelReport.dart';
import 'package:gtrack/model/overSpeedReport.dart';
import 'package:gtrack/model/overallReports.dart';
import 'package:gtrack/model/servicingReports.dart';
import 'package:gtrack/model/tripReport.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:gtrack/utils/internetConnectionUtils.dart';
import 'package:gtrack/utils/textUtils.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class OverallReportsAction {
  ThunkAction<AppState> overallReports(
      GlobalKey<ScaffoldState> key, DateTime fromDate, DateTime toDate) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio.get(TextUtils.overallReportUrl,
            options: Options(
              headers: {
                'api-key': TextUtils.apiKey,
                'access-token': store.state.user.token,
              },
            ),
            queryParameters: {
              'from_data': fromDate,
              'to_date': toDate,
            }).then((response) {
          final data = response.data['data'];

          List<OverallReports> overallReportsList = new List();

          data.forEach((item) {
            overallReportsList.add(OverallReports(
              item['name'],
              item['mileage'],
              item['driver'],
              (item['totalDistance'] ?? '').toString(),
              (item['finalDistance'] ?? '').toString(),
              (item['speedCount'] ?? '').toString(),
              (item['speedSum'] ?? '').toString(),
              (item['speedMax'] ?? '').toString(),
              (item['lastSpeed'] ?? '').toString(),
              (item['overSpeedCount'] ?? '').toString(),
              item['isInOverSpeed'],
              (item['overspeedLimit'] ?? '').toString(),
              (item['initailDistance'] ?? '').toString(),
              item['deviceId'],
              (item['fuelConsumption'] ?? '').toString(),
            ));
          });

          store.dispatch(
              OverallReportsFetched(overallReportsList: overallReportsList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class OverallReportsFetched {
  List<OverallReports> overallReportsList;
  OverallReportsFetched({this.overallReportsList});
}

class DistanceFuelReportAction {
  ThunkAction<AppState> distanceFuelReport(
      GlobalKey<ScaffoldState> key, DateTime fromDate, DateTime toDate) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio.get(TextUtils.distanceFuelReportUrl,
            options: Options(headers: {
              'api-key': TextUtils.apiKey,
              'access-token': store.state.user.token,
            }),
            queryParameters: {
              'from_date': fromDate,
              'to_date': toDate,
            }).then((response) {
          final data = response.data['data'];

          final reports = data['reports'];
          List<DistanceFuelReport> distanceFuelReportList = new List();
          reports.forEach((item) {
            distanceFuelReportList.add(DistanceFuelReport(
              item['targetName'],
              item['totalDistance'],
              (item['averageSpeed'] ?? '').toString(),
              (item['maxSpeed'] ?? '').toString(),
              (item['fuelConsumed'] ?? '').toString(),
              item['deviceId'],
              item['mileage'],
              item['vehicleType'],
            ));
          });

          store.dispatch(DistanceFuelReportFetched(
              distanceFuelReportList: distanceFuelReportList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class DistanceFuelReportFetched {
  List<DistanceFuelReport> distanceFuelReportList;
  DistanceFuelReportFetched({this.distanceFuelReportList});
}

class TripReportActions {
  ThunkAction<AppState> tripReports(GlobalKey<ScaffoldState> key,
      String deviceId, DateTime fromDate, DateTime toDate) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio
            .get(
          //  TextUtils.baseUrl + 'company/$deviceId/trip-report',
          TextUtils.baseUrl +
              'company/$deviceId/trip-report?_format=json&from_date=$fromDate&to_date=$toDate',
          options: Options(headers: {
            'api-key': TextUtils.apiKey,
            'access-token': store.state.user.token,
          }),
          // queryParameters: {
          //   'from_data': fromDate,
          //   'to_date': toDate,
          // },
        )
            .then((response) {
          final data = response.data['data'];

          List<TripReports> tripReportsList = new List();
          if (data.isNotEmpty) {
            data.keys.forEach((dates) {
              List<Trips> tripsList = new List();
              data[dates].forEach((item) {
                tripsList.add(Trips(
                  item['initial_time'] ?? '',
                  item['final_time'] ?? '',
                  item['total_time'] ?? '',
                  item['distance'] ?? '',
                  item['initial_location'] ?? '',
                  item['final_location'] ?? '',
                ));
              });
              tripReportsList.add(TripReports(dates, tripsList));
            });
          }

          store.dispatch(TripReportsFetched(tripReportsList: tripReportsList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class TripReportsFetched {
  List<TripReports> tripReportsList;
  TripReportsFetched({this.tripReportsList});
}

class OverSpeedReportsAction {
  ThunkAction<AppState> overSpeedReports(GlobalKey<ScaffoldState> key,
      String deviceId, DateTime fromDate, DateTime toDate) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio
            .get(
          TextUtils.baseUrl +
              'company/$deviceId/overspeed-report?_format=json&from_date=$fromDate&to_date=$toDate',
          options: Options(headers: {
            'api-key': TextUtils.apiKey,
            'access-token': store.state.user.token,
          }),
        )
            .then((response) {
          final data = response.data['data'];

          List<OverSpeedReports> overSpeedReportsList = new List();

          if (data.isNotEmpty) {
            data.keys.forEach((dates) {
              List<OverSpeed> overSpeedList = new List();
              data[dates].forEach((item) {
                overSpeedList.add(OverSpeed(
                  item['speed'] ?? '',
                  item['initial_time'] ?? '',
                  item['final_time'] ?? '',
                  item['initial_location'] ?? '',
                  item['total_distance'] ?? '',
                  item['total_time'] ?? '',
                ));
              });
              overSpeedReportsList.add(OverSpeedReports(dates, overSpeedList));
            });
          }

          store.dispatch(OverspeedReportsFetched(
              overSpeedReportsList: overSpeedReportsList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class OverspeedReportsFetched {
  List<OverSpeedReports> overSpeedReportsList;
  OverspeedReportsFetched({this.overSpeedReportsList});
}

class ServicingReportsAction {
  ThunkAction<AppState> servicingReports(GlobalKey<ScaffoldState> key,
      String deviceId, DateTime fromDate, DateTime toDate) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio.get(TextUtils.baseUrl + 'company/' + deviceId + '/servicing-report',
            options: Options(headers: {
              'api-key': TextUtils.apiKey,
              'access-token': store.state.user.token,
            }),
            queryParameters: {
              'from_date': fromDate,
              'to_date': toDate,
            }).then((response) {
          final data = response.data['data'];

          List<ServicingReports> servicingReportsList = new List();
          data.forEach((item) {
            servicingReportsList.add(ServicingReports(
              item['id'],
              item['vehicleName'],
              item['price'],
              item['billNumber'],
              item['remarks'],
              (item['distance'] ?? '').toString(),
              item['type'],
              item['date'],
            ));
          });
          store.dispatch(ServicingReportsFetched(
              servicingReportsList: servicingReportsList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class ServicingReportsFetched {
  List<ServicingReports> servicingReportsList;
  ServicingReportsFetched({this.servicingReportsList});
}
