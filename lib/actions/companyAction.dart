// Playback route history
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/department.dart';
import 'package:gtrack/model/vehicle.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:gtrack/utils/internetConnectionUtils.dart';
import 'package:gtrack/utils/textUtils.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class VehicleRoutesAction {
  ThunkAction<AppState> historyRoutes(GlobalKey<ScaffoldState> key,
      String vehicleId, DateTime fromDate, DateTime toDate) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio
            .get(
          TextUtils.baseUrl +
              'company/$vehicleId/positions?_format=json&from_date=$fromDate&to_date=$toDate',
          options: Options(
            headers: {
              'api-key': TextUtils.apiKey,
              'access-token': store.state.user.token,
            },
          ),
        )
            .then((response) {
          final data = response.data['data'];

          final positions = data['positions'];
          print("Positions : $positions");

          List<VehiclePosition> routeHistoryList = new List();
          if (positions.isNotEmpty) {
            positions.forEach((item) {
              routeHistoryList.add(VehiclePosition(
                item['overSpeed'],
                item['latitude'],
                item['longitude'],
                item['course'],
                item['speed'],
                item['address'],
                item['deviceTime'],
              ));
            });
          }

          store.dispatch(
              RouteHistoryFetched(routeHistoryList: routeHistoryList));
        }).catchError((error) {
          print("Error : $error");
          TextUtils.navigatorKey.currentState.pop();
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class RouteHistoryFetched {
  List<VehiclePosition> routeHistoryList;
  RouteHistoryFetched({this.routeHistoryList});
}

// departments
class DepartmentAction {
  ThunkAction<AppState> companyDepartments(GlobalKey<ScaffoldState> key) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();

      if (isConnected) {
        Dio dio = Dio();
        await dio
            .get(
          TextUtils.companyDepartmentsUrl,
          options: Options(
            headers: {
              'api-key': TextUtils.apiKey,
              'access-token': store.state.user.token,
            },
          ),
        )
            .then((response) {
          final data = response.data['data'];
          final departments = data['departments'];

          List<Department> departmentList = new List();

          departments.forEach((dept) {
            var vehicles = dept['vehicles'];
            List<DepartmentVehicles> departmentVehiclesList = new List();
            vehicles.forEach((item) {
              departmentVehiclesList.add(
                DepartmentVehicles(
                  item['id'],
                  (item['number'] ?? '').toString(),
                  (item['code'] ?? '').toString(),
                  item['deviceId'],
                  (item['deviceUniqueId'] ?? '').toString(),
                  (item['deviceName'] ?? '').toString(),
                  (item['deviceStatus'] ?? '').toString(),
                  (item['vehicleType'] ?? '').toString(),
                  (item['vehicleTypeImage'] ?? '').toString(),
                  (item['overSpeedLimit'] ?? '').toString(),
                  (item['lastUpdate'] ?? '').toString(),
                  (item['longitude'] ?? '').toString(),
                  (item['latitude'] ?? '').toString(),
                  (item['address'] ?? '').toString(),
                  (item['speed'] ?? '').toString(),
                  (item['ignition'] ?? '').toString(),
                  (item['charge'] ?? '').toString(),
                  (item['battery'] ?? '').toString(),
                  (item['rssi'] ?? '').toString(),
                  (item['course'] ?? '').toString(),
                  (item['alarm'] ?? '').toString(),
                  (item['lastPositionTime'] ?? '').toString(),
                  (item['event'] ?? '').toString(),
                  (item['lastEventTime'] ?? '').toString(),
                  false,
                ),
              );
            });
            departmentList.add(
              Department(dept['name'], departmentVehiclesList),
            );
          });

          store.dispatch(
              DepartmentVehiclesFetched(departmentList: departmentList));
          // store.dispatch(
          //     DepartmentVehiclesFetched(departmentList: departmentList));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class DepartmentVehiclesFetched {
  List<Department> departmentList;
  DepartmentVehiclesFetched({this.departmentList});
}
