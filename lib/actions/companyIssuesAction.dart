import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/issues.dart';
import 'package:gtrack/utils/dialoguesUtils.dart';
import 'package:gtrack/utils/internetConnectionUtils.dart';
import 'package:gtrack/utils/textUtils.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class IssuesAction {
  ThunkAction<AppState> issues(GlobalKey<ScaffoldState> key) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
        Dio dio = Dio();
        dio
            .get(TextUtils.issuesUrl,
                options: Options(headers: {
                  'api-key': TextUtils.apiKey,
                  'access-token': store.state.user.token,
                }))
            .then((response) {
          final data = response.data['data'];

          final issues = data['issues'];
          List<Issues> issuesList = new List();
          issues.forEach((item) {
            issuesList.add(Issues(
              item['id'],
              item['issueNumber'],
              item['title'],
              item['description'],
              item['department'],
              item['vehicle'],
              item['status'],
              item['created'],
              item['detailUrl'],
            ));
          });
          Issue issue = new Issue(
            issuesList,
            data['viewAllLink'],
            data['currentPage'],
            data['nextPage'],
            data['previousPage'],
            data['total'],
          );
          store.dispatch(IssueFetched(issue: issue));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class IssueFetched {
  Issue issue;
  IssueFetched({this.issue});
}

class CompanyIssueAction {
  ThunkAction<AppState> companyIssue(GlobalKey<ScaffoldState> key) {
    return (Store<AppState> store) async {
      bool isConnected = await InternetConnection.checkConnection();
      if (isConnected) {
       
        Dio dio = Dio();
        dio
            .get(TextUtils.companyIssuesUrl + '/9',
                options: Options(headers: {
                  'api-key': TextUtils.apiKey,
                  'access-token': store.state.user.token,
                }))
            .then((response) {
          final data = response.data['data'];

          final issue = data['issue'];
          final comments = data['comments'];

          List<Comments> issuesCommentsList = new List();

          Issues issues = new Issues(
            issue['id'],
            issue['issueNumber'],
            issue['title'],
            issue['description'],
            issue['department'],
            issue['vehicle'],
            issue['status'],
            issue['created'],
            '',
          );

          comments.forEach((item) {
            issuesCommentsList.add(Comments(
              item['id'],
              item['comment'],
              item['created'],
              item['commentator'],
            ));
          });

          CompanyIssue commpanyIssue = CompanyIssue(issues, issuesCommentsList);

          store.dispatch(CompanyIssueFetched(companyIssue: commpanyIssue));
        }).catchError((error) {
          DialoguesUtils.showSnackBar(key, error.response.data['message']);
        });
      } else {
        DialoguesUtils.showSnackBar(key, 'No Internet Connection');
      }
    };
  }
}

class CompanyIssueFetched {
  CompanyIssue companyIssue;
  CompanyIssueFetched({this.companyIssue});
}
