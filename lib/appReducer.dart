import 'package:gtrack/actions/alertsAction.dart';
import 'package:gtrack/actions/companyAction.dart';
import 'package:gtrack/actions/companyIssuesAction.dart';
import 'package:gtrack/actions/disposeActions.dart';
import 'package:gtrack/actions/loginAction.dart';
import 'package:gtrack/actions/reportsAction.dart';
import 'package:gtrack/appState.dart';
import 'package:gtrack/model/user.dart';

AppState appReducer(AppState state, action) {
  // login
  if (action is UserLoggedIn) {
   
    return state.copyWith(user: new Nullable(action.user));
  }
  // logout
  else if (action == Logout.LogoutUser) {
    return state.copyWith(user: new Nullable<User>(null));
  }

  // department vehicles
  else if (action is DepartmentVehiclesFetched) {
   
    return state.copyWith(departmentList: action.departmentList);
  }
  //route history
  else if (action is RouteHistoryFetched) {
    return state.copyWith(routeHistoryList: action.routeHistoryList);
  }
  // issues
  else if (action is IssueFetched) {
    return state.copyWith(issue: action.issue);
  }

  // company issues
  else if (action is CompanyIssueFetched) {
    return state.copyWith(companyIssue: action.companyIssue);
  }

  // maintenance alerts
  else if (action is MaintenanceAlertsFetched) {
    return state.copyWith(maintenanceAlertsList: action.maintenanceAlertsList);
  }

  //event alerts
  else if (action is EventAlertsFetched) {
    return state.copyWith(eventAlerts: action.eventAlerts);
  }

  // overall reports
  else if (action is OverallReportsFetched) {
    return state.copyWith(
      overallReportsList: action.overallReportsList,
      distanceFuelReportList: [],
      tripReportsList: [],
      overSpeedReportsList: [],
      servicingReportsList: [],
    );
  }

  // distance-fuel-report
  else if (action is DistanceFuelReportFetched) {
    return state.copyWith(
      overallReportsList: [],
      distanceFuelReportList: action.distanceFuelReportList,
      tripReportsList: [],
      overSpeedReportsList: [],
      servicingReportsList: [],
    );
  }

  // trip reports
  else if (action is TripReportsFetched) {
    return state.copyWith(
      overallReportsList: [],
      distanceFuelReportList: [],
      tripReportsList: action.tripReportsList,
      overSpeedReportsList: [],
      servicingReportsList: [],
    );
  }

  //overspeed reports
  else if (action is OverspeedReportsFetched) {
    return state.copyWith(
      overallReportsList: [],
      distanceFuelReportList: [],
      tripReportsList: [],
      overSpeedReportsList: action.overSpeedReportsList,
      servicingReportsList: [],
    );
  }

  //servicing reports
  else if (action is ServicingReportsFetched) {
    return state.copyWith(
      overallReportsList: [],
      distanceFuelReportList: [],
      tripReportsList: [],
      overSpeedReportsList: [],
      servicingReportsList: action.servicingReportsList,
    );
  }

  // dispose actions
  else if (action == DisposeActions.DisposeRouteHistoryList) {
    return state.copyWith(routeHistoryList: null);
  } else if (action == DisposeActions.DisposeDepartmentList) {
    return state.copyWith(departmentList: []);
  } else if (action == DisposeActions.DisposeOverallReportsList) {
    return state.copyWith(overallReportsList: null);
  } else if (action == DisposeActions.DisposeDistanceFuelReportsList) {
    return state.copyWith(distanceFuelReportList: null);
  } else if (action == DisposeActions.DisposeTripReportsList) {
    return state.copyWith(tripReportsList: null);
  } else if (action == DisposeActions.DisposeOverSpeedReportsList) {
    return state.copyWith(overSpeedReportsList: null);
  } else if (action == DisposeActions.DisposeServicingReportsList) {
    return state.copyWith(servicingReportsList: null);
  } else if (action == DisposeActions.DisposeIssue) {
    return state.copyWith(issue: null);
  } else if(action == DisposeActions.DisposeMaintenanceAlertsList){
    return state.copyWith(maintenanceAlertsList: null);
  } else if(action == DisposeActions.DisposeEventAlerts){
    return state.copyWith(eventAlerts: null);
  }
  else {
    return state;
  }
}

/**
 *   LogoutUser,
  DisposeRouteHistoryList,
  DisposeDepartmentList,
  DisposeIssue,
  DisposeCompanyIssue,
  DisposeMaintenanceAlertsList,
  DisposeEventAlerts,
  DisposeOverallReportsList,
  DisposeDistanceFuelReportsList,
  DisposeTripReportsList,
  DisposeOverSpeedReportsList,
  DisposeServicingReportsList,
 */
