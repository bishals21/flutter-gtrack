import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';
import 'dart:convert';

class WebSocketDemo extends StatefulWidget {
  @override
  _WebSocketDemoState createState() => _WebSocketDemoState();
}

class _WebSocketDemoState extends State<WebSocketDemo> {
  IOWebSocketChannel channel;
  List<String> list = new List();

  @override
  void dispose() {
    channel.sink.close();
    super.dispose();
  }

  @override
  void initState() {
   
    channel = IOWebSocketChannel.connect(
        'ws://206.189.234.105:4001/gps-server/api/socket',
        pingInterval: Duration(seconds: 2));
    
    channel.sink.add('866968030512330');
    channel.stream.listen((data) {
      setState(() {
        final jsonResponse = json.decode(data);
        print('Data : $jsonResponse');

        final latitude = jsonResponse['positions']['latitude'];
        final longitude = jsonResponse['positions']['longitude'];
        print(
            'Latitude : ${latitude.toString()} Longitude : ${longitude.toString()}');
      });
    });

  
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('Web Socket Demo'),
      ),
      body: Column(
        children: list.map((data) => Text(data)).toList(),
      ),
    );
  }
}
